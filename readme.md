# Net4Care PHMR Library

This component allows

   - A *GreenCDA* approach for constructing a data object representing
     a PHMR document: *SimpleClinicalDocument*
   - A Builder which can produce a XML document representing a valid
     Danish PHMR (following the MedCom profile) document from the
     *SimpleClinicalDocument* object.

## Usage

Normally, you should *not* clone this repository *unless* you are actively
developing on the PHMR builder. Instead you should fetch the module
from the public maven repository that we provide!

Thus, if you are just going to use the PHMR builder, do not read on
here, but head over to the [Net4Care PHMR
Tutorial](https://bitbucket.org/4s/net4care-phmr-tutorial) instead!


## Prerequisites
 - Git 
 - Java version 1.6, or newer
 - Maven version 3.0, or newer

## Build
 - Clone project from Bitbucket:  
   `git clone https://bitbucket.org/4s/net4care-phmr-builder.git`
 - Compile and run tests:  
   `mvn install`

## Develop

### Eclipse

- Prepare eclipse project files:  
  `mvn eclipse:eclipse`
- If the M2_REPO variable is not set in eclipse then execute:  
  `mvn -Declipse.workspace=<path to eclipse workspace>; eclipse:configure-workspace`
- Open eclipse and import as existing project, see [eclipse help](http://help.eclipse.org/kepler/index.jsp?topic=/org.eclipse.platform.doc.user/tasks/tasks-importproject.htm)

## Deploy

Only maintainers of the repository should do this, provided here for reference

 - Add artifactory credentials to *$HOME/.m2/settings.xml*
 - Verify that the pom.xml has the proper version number (No SNAPSHOT) and
   that changes.md is updated
 - Execute: `mvn deploy -P {CS|4S}`

Use profile `CS` for [AU CS's Twiga repository](http://twiga.cs.au.dk:8081/artifactory/simple/libs-release-net4care/) 
and `4S` for [4S' repository](http://artifactory.4s-online.dk/artifactory/libs-snapshot-local)


  