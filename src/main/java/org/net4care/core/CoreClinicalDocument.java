package org.net4care.core;

import java.util.Date;

import org.net4care.phmr.model.*;

public interface CoreClinicalDocument {

  /** Define when the document was created. 
   * 
   * @param documentCreationTime The document creation time.
   */
  public void setEffectiveTime(Date documentCreationTime);

  /** Define a unique identifier (CDA Release 2, §4.2.1.7 ClinicalDocument.setId /
   * "Represents an identifier that is common across all document revisions")
   * and the version number (CDA Release 2, §4.2.1.8 ClinicalDocument.versionNumber /
   * "An integer value used to version successive replacement documents".
   * Normally for PHMR documents just use a unique identifier, and version number
   * 1.
   * @param uniqueId the identifier for the document
   * @param versionNumber the number of this version
   */
  public void setDocumentVersion(String uniqueId, int versionNumber);

  /** Define who is the patient.
   * 
   *  @param patientIdentity The patient identity as a PersonIdentity.
   *  */
  public void setPatient(PersonIdentity patientIdentity);

  /** Define who is the author of the document, i.e.
   * the organization and person and time when
   * the author participated in creating the document.
   * @param authorOrganizationIdentity The author organization identity.
   * @param authorPersonIdentity The author person identity.
   * @param authorParticipationStart The author participation start.
   */
  public void setAuthor(OrganizationIdentity authorOrganizationIdentity,
      PersonIdentity authorPersonIdentity, Date authorParticipationStart);

  /** Set the organization that is custodian of the document.
   * 
   *  @param custodianIdentity The custodian organization identity.
   *  */
  public void setCustodian(OrganizationIdentity custodianIdentity);

  /** Set the organization, person, and time of authentication.
   * 
   *  @param authenticatorOrganizationIdentity The authenticator organization identity.
   *  @param authenticatorPersonIdentity The authenticator person identity.
   *  @param timeOfAuthentication The time of authentication.
   *  */
  public void setAuthenticator(
      OrganizationIdentity authenticatorOrganizationIdentity,
      PersonIdentity authenticatorPersonIdentity, Date timeOfAuthentication);

  /** Set the time interval that the measurements span.
   * 
   *  @param from The earliest time in the time interval.
   *  @param to The latest time in the time interval.
   *  */
  public void setDocumentationTimeInterval(Date from, Date to);

  /** Given a builder instance, construct a representation of this object.
   * 
   * @param builder A valid builder instance
   */
  public void construct(ClinicalDocumentBuilder builder);
}