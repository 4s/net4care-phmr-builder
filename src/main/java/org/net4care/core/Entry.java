package org.net4care.core;

import org.net4care.phmr.model.Observation;

public class Entry {

  private Observation observation;
  
  public Observation createObservation() {
    observation = new Observation();
    return observation;
  }

  public Observation getObservation() {
    return observation;
  }

  @Override
  public String toString() {
    return "Entry [observation=" + observation + "]";
  }

}
