package org.net4care.core;

import org.hl7.v3.*;
import org.w3c.dom.Document;

/** A section extender that does nothing.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public class NullSectionExtender implements SectionExtender {

  @Override
  public void extend(Section section, POCDMT000040Component3 POCDComponent, 
      ObjectFactory factory, Document rootDocument) {
    // Do nothing...
  }

}
