package org.net4care.core;

import java.util.*;

public class Section {

  private List<Entry> entryList = new ArrayList<Entry>();
  
  public Entry createEntry() {
    Entry entry = new Entry();
    entryList.add(entry);
    return entry;
  }

  public List<Entry> getEntryList() {
    return entryList;
  }

  @Override
  public String toString() {
    return "Section [entry=" + entryList + "]";
  }

}
