package org.net4care.core;

import org.hl7.v3.*;
import org.w3c.dom.Document;

/** This interface defines a call back from the builder allowing
 * the developer to 'post process' a structured body section after
 * the builder has completed its own building process.
 * 
 * This way a developer can add more tags and information to
 * the section which is not directly supported by the builder.
 * This is especially true for the QFDD builder which is
 * presently rather limited in expressiveness.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public interface SectionExtender {

  /** Call back from the builder, invoked for every section in the
   * document model
   * @param section the document model section now being processed
   * @param POCDComponent the XML/POCD document that will go into
   * the final XML output - you should add your tags to this
   * to get it into the final XML output.
   * @param docBuilder 
   */
  void extend(Section section, POCDMT000040Component3 POCDComponent, 
      ObjectFactory factory, Document rootDocument);

}
