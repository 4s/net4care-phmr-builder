package org.net4care.bro;

import java.util.*;

import org.net4care.phmr.codes.HL7;

public class QFDObservationMultipleChoice extends QFDObservationBase implements QFDObservation {

  private List<CodeValue> answerOptionList;
  
  public QFDObservationMultipleChoice(String question, String code, String codeSystem, String displayName, String codeSystemName) {
    super(question, "pending", "pending", "pending", code, codeSystem, displayName, codeSystemName);
    answerOptionList = new ArrayList<CodeValue>();
  }

  
  public String toString() {
    return getQuestion();
  }

  @Override
  public String getTypeOID() {
    return HL7.QFD_MULTIPLE_CHOICE_QUESTION_PATTERN_OID;
  }


  public void addAnswerOption(String code, String codeSystem, String displayName,
      String codeSystemName) {
    answerOptionList.add(new CodeValue(code, codeSystem, displayName, codeSystemName));
  }


  public List<CodeValue> getAnswerOptionList() {
    return answerOptionList;
  }

}
