package org.net4care.bro;

import java.util.*;

public class QFDSection {

  @Override
  public String toString() {
    return "QFDSection [title=" + title + ", text=" + text + ", obsList="
        + obsList + "]";
  }

  private String title;
  private String text;
  private List<QFDObservation> obsList;

  public QFDSection(String title, String text) {
    this.title = title; this.text = text;
    obsList = new ArrayList<QFDObservation>();
  }

  public String getTitle() {
    return title;
  }

  public String getText() {
    return text;
  }

  public void addObservation(QFDObservation qfdQuestion) {
    obsList.add(qfdQuestion);
  }

  public List<QFDObservation> getObsList() {
    return obsList;
  }


}
