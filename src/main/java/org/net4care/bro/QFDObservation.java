package org.net4care.bro;

public interface QFDObservation {

  String getTypeOID();
  
  String getQuestion();

  String getDisplayName();

  String getCodeSystemName();

  String getCodeSystem();

  String getCode();

  String getIdExtension();

  String getIdRoot();

  String getIdAuthorityName();
}
