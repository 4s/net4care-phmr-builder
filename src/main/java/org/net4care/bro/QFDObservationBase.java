package org.net4care.bro;

import org.net4care.phmr.codes.HL7;

public class QFDObservationBase implements QFDObservation {

  protected String question;
  private CodeValue theValue;
  private String idExtension;
  private String idRoot;
  private String idAuthorityName;

  public QFDObservationBase(String question, 
      String idExtension, String idRoot, String idAssigningAuthorityName,
      String code, String codeSystem, 
      String displayName, String codeSystemName) {
    this.question = question;
    this.theValue = new CodeValue(code, codeSystem, displayName, codeSystemName);
    this.idExtension = idExtension;
    this.idRoot = idRoot;
    this.idAuthorityName = idAssigningAuthorityName;
  }

  @Override
  public String getQuestion() {
    return question;
  }

  @Override
  public String getCode() {
    return theValue.getCode();
  }

  @Override
  public String getCodeSystem() {
    return theValue.getCodeSystem();
  }

  @Override
  public String getCodeSystemName() {
    return theValue.getCodeSystemName();
  }

  @Override
  public String getDisplayName() {
    return theValue.getDisplayName();
  }

  @Override
  public String getTypeOID() {
    return HL7.QFD_TEXT_QUESTION_PATTERN_OID;
  }

  @Override
  public String toString() {
    return "QFDObservationBase [question=" + question + ", code=" + theValue.getCode()
        + ", codeSystem=" + theValue.getCodeSystem() + ", codeSystemName=" + theValue.getCodeSystemName()
        + ", displayName=" + theValue.getDisplayName() + "]";
  }

  public String getIdExtension() {
    return idExtension;
  }

  public String getIdRoot() {
    return idRoot;
  }

  public String getIdAuthorityName() {
    return idAuthorityName;
  }

}