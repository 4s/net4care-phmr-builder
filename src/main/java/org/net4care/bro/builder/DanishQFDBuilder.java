package org.net4care.bro.builder;

import java.math.BigInteger;
import java.util.*;

import javax.xml.bind.JAXBElement;

import org.hl7.v3.*;
import org.net4care.bro.*;
import org.net4care.core.*;
import org.net4care.phmr.builders.*;
import org.net4care.phmr.codes.*;
import org.net4care.phmr.model.*;
import org.w3c.dom.Element;

/** A builder for QFD according to the Danish standard.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public class DanishQFDBuilder extends BaseClinicalDocumentBuilder implements ClinicalDocumentBuilder {

  public DanishQFDBuilder() {
    super();
  }
  
  public DanishQFDBuilder(UUIDStrategy uUIDStrategy) {
    super(uUIDStrategy);
  }
  
  public DanishQFDBuilder(UUIDStrategy uUIDStrategy, SectionExtender sectionExtender) {
    super(uUIDStrategy, sectionExtender);
  }
  
  // Override the PHMRBuilders docclin and EVN attributes
  @Override
  public void buildRootNode() {
    result = factory.createPOCDMT000040ClinicalDocument();
  }
  
  @Override
  public void buildHeader(CDAHeaderData data) {
    // Add the realm code
    super.buildHeader(data);
    CS dk = factory.createCS();
    dk.setCode("DK");
    result.getRealmCode().add(dk);
  }

  @Override
  public void buildContext(String theTitle, PersonIdentity patientIdentity, Date effectiveTime,
                           String setId, String versionNumber) {
    // TODO: Temporary copy'n'paste from PHMR builder, abstract into superclass or use composition
    // CONF-PHMR-15
    STExplicit title = factory.createSTExplicit();
    title.getContent().add(theTitle);
    result.setTitle(title);

    // CONF-PHMR-16 / CONF-DK PHMR-17
    TSExplicit effectiveTimeTsExplicit = factory.createTSExplicit();
    effectiveTimeTsExplicit.setValue(dateFormatter.format(effectiveTime));
    result.setEffectiveTime(effectiveTimeTsExplicit);

    // Section 2.9
    CE confidentialityCode = createCode("N",HL7.CONFIDENTIALITY_OID);
    result.setConfidentialityCode(confidentialityCode);

    // CONF-PHMR-17-20
    CS languageCode = factory.createCS();
    languageCode.setCode("da-DK");
    result.setLanguageCode(languageCode);
  }
  
  @Override
  public void buildDocumentationOf(Date serviceStart, Date serviceEnd) {
    // Not used in QFDD
  }
  
  
  @Override
  public void buildSections(List<Section> sectionList) {
    // TODO: The loop missing here!!!
    
    int i = 0;
    // for
    Section result = sectionList.get(i);
    Observation obs = result.getEntryList().get(i).getObservation();
    QFDSection qfdSec = (QFDSection) obs.getContents().get("QFDSection");
    
    POCDMT000040Component3 pocdSection = createASection(qfdSec);
    
    sectionExtender.extend(result, pocdSection, factory, docContextForCreatingTextSections);
    
    this.result.getComponent().getStructuredBody().getComponent().add(pocdSection);
    
    // end for
  }
  
  @Override
  public void buildCopyrightSection(String theCopyrightText) {
    POCDMT000040Component3 copyrightComponent = factory.createPOCDMT000040Component3();
    copyrightComponent.setTypeCode(ActRelationshipHasComponent.COMP);
    copyrightComponent.setContextConductionInd(Boolean.TRUE);

    result.getComponent().getStructuredBody().getComponent().add(copyrightComponent);

    // Section
    POCDMT000040Section section = factory.createPOCDMT000040Section();
    section.getClassCode().add("DOCSECT");
    section.getMoodCode().add("EVN");

    copyrightComponent.setSection(section);

    STExplicit title = factory.createSTExplicit();
    title.getContent().add("Copyright section");
    section.setTitle(title);
    
    II ii = factory.createII();
    ii.setRoot(HL7.QFD_COPYRIGHT_SECTION_TEMPLATEID);
    section.getTemplateId().add(ii);
    
    Element textElement = docContextForCreatingTextSections.createElement("text");
    textElement.setAttribute("xmlns", "urn:hl7-org:v3");
    // no contents
    // textElement.setTextContent(theCopyrightText);
    section.setText(textElement);
    
    // language code
    CS value = factory.createCS();
    value.setCode("en-US");
    section.setLanguageCode(value);
    
    POCDMT000040Entry entry = factory.createPOCDMT000040Entry();
    entry.setTypeCode(XActRelationshipEntry.DRIV);
    section.getEntry().add(entry);
    
    // Copyright pattern
    POCDMT000040Observation observation = factory.createPOCDMT000040Observation();
    observation.getClassCode().add("OBS");
    observation.setMoodCode(XActMoodDocumentObservation.EVN);
    entry.setObservation(observation);

    II ii2 = factory.createII();
    ii2.setRoot(HL7.QFD_COPYRIGHT_PATTERN_TEMPLATEID);
    observation.getTemplateId().add(ii2);
    
    CDExplicit theCode = factory.createCDExplicit();
    observation.setCode(theCode);
    
    theCode.setCode("COPY");
    theCode.setCodeSystem(Loinc.OID);
    theCode.setDisplayName("Code for Copyright");
    theCode.setCodeSystemName(Loinc.DISPLAYNAME);

    EDExplicit theQuestion = factory.createEDExplicit();
    theQuestion.getContent().add(theCopyrightText);

    theCode.setOriginalText(theQuestion);
  }

  private POCDMT000040Component3 createASection(QFDSection qfdsection) {
    POCDMT000040Component3 retVal = factory.createPOCDMT000040Component3();
    retVal.setTypeCode(ActRelationshipHasComponent.COMP);
    retVal.setContextConductionInd(Boolean.TRUE);

    POCDMT000040Section section = factory.createPOCDMT000040Section();// new POCDMT000040Section();
    section.getClassCode().add("DOCSECT");
    section.getMoodCode().add("EVN");

    retVal.setSection(section);

    // Create template id
    II ii = factory.createII();
    ii.setRoot(HL7.QFD_SECTION_ROOT_OID);
    section.getTemplateId().add(ii);

    // Create code
    CE sectionCode = factory.createCE();
    section.setCode(sectionCode);
    sectionCode.setCode(Loinc.QFD_CODE);
    sectionCode.setCodeSystem(Loinc.OID);
    sectionCode.setCodeSystemName(Loinc.DISPLAYNAME);
    
    String titelString = qfdsection.getTitle();
    section.setTitle(generateTitle(titelString));

    Element textElement = docContextForCreatingTextSections.createElement("text");
    textElement.setAttribute("xmlns", "urn:hl7-org:v3");
    textElement.setTextContent(qfdsection.getText());
    section.setText(textElement);
    
    // Entry section
    POCDMT000040Entry entry = null;

    entry = factory.createPOCDMT000040Entry();
    entry.setContextConductionInd(Boolean.TRUE);
    entry.setTypeCode(XActRelationshipEntry.DRIV);
    section.getEntry().add(entry);

    POCDMT000040Organizer organizer = factory.createPOCDMT000040Organizer();
    organizer.setClassCode(XActClassDocumentEntryOrganizer.BATTERY);
    organizer.getMoodCode().add(XDocumentActMood.EVN.value());
    entry.setOrganizer(organizer);
    
    II ii2 = factory.createII();
    ii2.setRoot(HL7.QUESTION_ORGANIZER);
    organizer.getTemplateId().add(ii2);

    CS cs = factory.createCS();
    cs.setCode("COMPLETED");
    organizer.setStatusCode(cs);
    
    // Loop over all observations in this section
    int count = 1;
    for ( QFDObservation qfdObservation : qfdsection.getObsList() ) {
      POCDMT000040Component4 component = factory.createPOCDMT000040Component4();
      organizer.getComponent().add(component);

      INT seqN = new INT(); BigInteger bi = BigInteger.valueOf(count);
      seqN.setValue(bi);
      component.setSequenceNumber(seqN);
      
      count++;

      POCDMT000040Observation observation = factory.createPOCDMT000040Observation();
      observation.getClassCode().add("OBS");
      observation.setMoodCode(XActMoodDocumentObservation.DEF);
      component.setObservation(observation);

      // template id of observation
      II ii3 = factory.createII();
      ii3.setRoot(qfdObservation.getTypeOID());
      observation.getTemplateId().add(ii3);

      // id of observation
      II id = factory.createII();
      id.setExtension(qfdObservation.getIdExtension());
      id.setRoot(qfdObservation.getIdRoot());
      id.setAssigningAuthorityName(qfdObservation.getIdAuthorityName());
      
      observation.getId().add(id);
      
      // The question text
      String theQuestionsText = qfdObservation.getQuestion();

      CDExplicit theCode = factory.createCDExplicit();

      theCode.setCode(qfdObservation.getCode());
      theCode.setCodeSystem(qfdObservation.getCodeSystem());
      theCode.setCodeSystemName(qfdObservation.getCodeSystemName());
      theCode.setDisplayName(qfdObservation.getDisplayName());

      observation.setCode(theCode);

      EDExplicit theQuestion = factory.createEDExplicit();
      theQuestion.getContent().add(theQuestionsText);

      theCode.setOriginalText(theQuestion);
      
      // Handle the different cases of question types
      
      // === Multiple Choice Questons
      if ( qfdObservation.getClass() == QFDObservationMultipleChoice.class ) {
        buildAnswerOption(observation, (QFDObservationMultipleChoice) qfdObservation);
      }
    }
    
    return retVal;
  }

  private void buildAnswerOption(POCDMT000040Observation observation,
      QFDObservationMultipleChoice qfdObservation) {
    for ( CodeValue answerOption : qfdObservation.getAnswerOptionList() ) {
      CE value = factory.createCE();
      value.setCode(answerOption.getCode());
      value.setCodeSystem(answerOption.getCodeSystem());
      value.setDisplayName(answerOption.getDisplayName());
      value.setCodeSystemName(answerOption.getCodeSystemName());

      observation.getValue().add(value);
    }
    
    // Build the interval of valid responses
    POCDMT000040EntryRelationship entryRel;
    
    // TODO: Parameterize the lower and upper bounds of answers acceptable
    entryRel = buildIntervalOfValidResponses(observation, 1, 1);
    observation.getEntryRelationship().add(entryRel);
  }


  private POCDMT000040EntryRelationship buildIntervalOfValidResponses(POCDMT000040Observation observation,
      int minimumResponses, int maximumReponses) {
    POCDMT000040EntryRelationship entryRel;
    entryRel = factory.createPOCDMT000040EntryRelationship();
    entryRel.setTypeCode(XActRelationshipEntryRelationship.SUBJ);
    
    POCDMT000040Observation questionOption = factory.createPOCDMT000040Observation();
    entryRel.setObservation(questionOption);
    questionOption.getClassCode().add("OBS");
    questionOption.setMoodCode(XActMoodDocumentObservation.EVN);
    
    // Template id
    II ii = factory.createII();
    ii.setRoot(HL7.QFD_OPTIONS_PATTERNS_TEMPLATEID);
    questionOption.getTemplateId().add(ii);
    
    // Code
    // TODO: Why is CE not supported ???
    CEExplicit code = factory.createCEExplicit();
    code.setCode("74467-2");
    code.setCodeSystem(Loinc.OID);
    questionOption.setCode(code);
    
    IVLINT interval = factory.createIVLINT();
    IVXBINT low = factory.createIVXBINT(); BigInteger l = BigInteger.valueOf(minimumResponses);
    low.setValue(l);
    JAXBElement<IVXBINT> arg0 = factory.createIVLINTLow(low);
    interval.getRest().add(arg0);

    IVXBINT high = factory.createIVXBINT(); BigInteger h = BigInteger.valueOf(maximumReponses);
    high.setValue(h);
    JAXBElement<IVXBINT> arg1 = factory.createIVLINTHigh(high);
    interval.getRest().add(arg1);

    questionOption.getValue().add(interval);
    
    return entryRel;
  }

  
}
