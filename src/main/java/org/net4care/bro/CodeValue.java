package org.net4care.bro;

public class CodeValue {
  private String code;
  private String codeSystem;
  private String codeSystemName;
  private String displayName;

  public CodeValue(String code2, String codeSystem2, String displayName2, String codeSystemName2) {
    code = code2;
    codeSystem = codeSystem2;
    displayName = displayName2;
    codeSystemName = codeSystemName2;
  }

  public String getCode() {
    return code;
  }

  public String getCodeSystem() {
    return codeSystem;
  }

  public String getCodeSystemName() {
    return codeSystemName;
  }

  public String getDisplayName() {
    return displayName;
  }
}