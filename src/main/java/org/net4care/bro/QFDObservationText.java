package org.net4care.bro;


public class QFDObservationText extends QFDObservationBase implements QFDObservation {

  public QFDObservationText(String question, String idExtension, String idRoot, String idAssigningAuthorityName, 
      String code, String codeSystem, String displayName, String codeSystemName) {
    super(question, idExtension, idRoot, idAssigningAuthorityName, code, codeSystem, displayName, codeSystemName);
  }
}
