package org.net4care.phmr.codes;

//CHECKSTYLE:OFF
public class HL7 {

  // International codes
  public static final String PHMR_DK_TYPEID_EXTENSION = "POCD_HD000040";
  public static final String PHMR_TYPEID_ROOT = "2.16.840.1.113883.1.3";

  public static final String CONFIDENTIALITY_OID = "2.16.840.1.113883.5.25";
  public static final String GENDER_OID = "2.16.840.1.113883.5.1";

  public static final String OBSERVATION_ORGANIZER = "2.16.840.1.113883.10.20.1.35";
  public static final String OBSERVATION = "2.16.840.1.113883.10.20.1.31";
  public static final String DEVICE_DEFINITION_ORGANIZER = "2.16.840.1.113883.10.20.9.4";
  public static final String PHMR_NUMERIC_OBSERVATION = "2.16.840.1.113883.10.20.9.8";
  public static final String PHMR_PRODUCT_INSTANCE = "2.16.840.1.113883.10.20.9.9";

  public static final String PHMR_VITAL_SIGNS_FIRST_ID = "2.16.840.1.113883.10.20.1.16";
  public static final String PHMR_VITAL_SIGNS_SECOND_ID = "1.2.208.184.11.1";

  public static final String PHMR_RESULTS_FIRST_ID = "2.16.840.1.113883.10.20.1.14";
  public static final String PHMR_RESULTS_SECOND_ID = "1.2.208.184.11.1";

  public static final String PHMR_MEDICAL_EQUIPMENT_FIRST_ID = "2.16.840.1.113883.10.20.1.7";
  public static final String PHMR_MEDICAL_EQUIPMENT_SECOND_ID = "1.2.208.184.11.1";

  public static final String PHMR_ENTRY_ID = "1.2.208.184.11.1";


  public static final String QFD_SECTION_ROOT_OID = "2.16.840.1.113883.10.20.32.2.1";
  public static final String QUESTION_ORGANIZER = "2.16.840.1.113883.10.20.32.4.1";
  public static final String QFD_TEXT_QUESTION_PATTERN_OID = "2.16.840.1.113883.10.20.32.4.9";
  public static final String QFD_MULTIPLE_CHOICE_QUESTION_PATTERN_OID = "2.16.840.1.113883.10.20.32.4.8";

  public static final String QFD_COPYRIGHT_SECTION_TEMPLATEID = "2.16.840.1.113883.10.20.32.2.2";
  public static final String QFD_COPYRIGHT_PATTERN_TEMPLATEID = "2.16.840.1.113883.10.20.32.4.21";
  public static final String QFD_OPTIONS_PATTERNS_TEMPLATEID = "2.16.840.1.113883.10.20.32.4.20";

}
