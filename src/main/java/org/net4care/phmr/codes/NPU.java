package org.net4care.phmr.codes;

import java.util.Date;

import org.net4care.phmr.model.*;

//CHECKSTYLE:OFF
public class NPU {
  // Often used codes in NPU for telemedical measurements


  public static final String DISPLAYNAME     = "NPU terminologien";
  public static final String CODESYSTEM_OID  = "1.2.208.176.2.1";

  public static final String DRY_BODY_WEIGHT_CODE = "NPU03804";
  public static final String DRY_BODY_WEIGHT_DISPLAYNAME = "Legeme masse; Pt";
  
  public static final String BLOOD_PRESURE_SYSTOLIC_ARM_CODE = "DNK05472";
  public static final String BLOOD_PRESURE_SYSTOLIC_ARM_DISPLAYNAME = "Blodtryk systolisk; Arm";
  
  public static final String BLOOD_PRESURE_DIASTOLIC_ARM_CODE = "DNK05473";
  public static final String BLOOD_PRESURE_DIASTOLIC_ARM_DISPLAYNAME = "Blodtryk diastolisk; Arm";
  
  public static final String PROTEIN_URINE_CODE = "NPU03958";
  public static final String PROTEIN_URINE_DISPLAYNAME = "Protein;U";
  
  public static final String SATURATION_CODE = "NPU03011";
  public static final String SATURATION_DISPLAYNAME = "O2 sat.;Hb(aB)";
  

  public static Measurement createWeight(String weightInKg, Date atTime) {
    Measurement meas;
    meas = new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED).
        setPhysicalQuantity(weightInKg, UCUM.kg, DRY_BODY_WEIGHT_CODE, DRY_BODY_WEIGHT_DISPLAYNAME).
        build();
    return meas; 
  }

  public static Measurement createWeight(String weightInKg, Date atTime, Context context) {
    Measurement meas;
    meas = new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED).
        setPhysicalQuantity(weightInKg, UCUM.kg, DRY_BODY_WEIGHT_CODE, DRY_BODY_WEIGHT_DISPLAYNAME).
        setContext(context).
        build();
    return meas; 
  }

  public static Measurement createBloodPresureSystolic(String systolic, Date atTime) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED).
        setPhysicalQuantity(systolic, UCUM.mmHg, BLOOD_PRESURE_SYSTOLIC_ARM_CODE, NPU.BLOOD_PRESURE_SYSTOLIC_ARM_DISPLAYNAME).
        build();
  }
  public static Measurement createBloodPresureSystolic(String systolic, Date atTime, Context context) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED).
        setPhysicalQuantity(systolic, UCUM.mmHg, BLOOD_PRESURE_SYSTOLIC_ARM_CODE, NPU.BLOOD_PRESURE_SYSTOLIC_ARM_DISPLAYNAME).
        setContext(context).
        build();
  }

  public static Measurement createBloodPresureDiastolic(String diastolic, Date atTime) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED).
        setPhysicalQuantity(diastolic, UCUM.mmHg, BLOOD_PRESURE_DIASTOLIC_ARM_CODE, BLOOD_PRESURE_DIASTOLIC_ARM_DISPLAYNAME).
        build();
  }
  public static Measurement createBloodPresureDiastolic(String diastolic, Date atTime, Context context) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED).
        setPhysicalQuantity(diastolic, UCUM.mmHg, BLOOD_PRESURE_DIASTOLIC_ARM_CODE, BLOOD_PRESURE_DIASTOLIC_ARM_DISPLAYNAME).
        setContext(context).
        build();
  }

  public static Measurement createProteinUrine(String protein, Date atTime) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED).
        setPhysicalQuantity(protein, UCUM.gPerL, PROTEIN_URINE_CODE, PROTEIN_URINE_DISPLAYNAME).
        build();
  }
  public static Measurement createProteinUrine(String protein, Date atTime, Context context) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED).
        setPhysicalQuantity(protein, UCUM.gPerL, PROTEIN_URINE_CODE, PROTEIN_URINE_DISPLAYNAME).
        setContext(context).
        build();
  }

  public static Measurement createSaturation(String saturation, Date atTime) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED).
        setPhysicalQuantity(saturation, UCUM.pct, SATURATION_CODE, SATURATION_DISPLAYNAME).
        build();
  }
  public static Measurement createSaturation(String saturation, Date atTime, Context context) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED).
        setPhysicalQuantity(saturation, UCUM.pct, SATURATION_CODE, SATURATION_DISPLAYNAME).
        setContext(context).
        build();
  }
 
  public static Measurement createCTG(String id, String reference, Date atTime, Context context) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED).
        setObservationMediaReference(id, reference).
        setContext(context).
        build();
  }
}
