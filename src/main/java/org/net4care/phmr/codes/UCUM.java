package org.net4care.phmr.codes;

/** A collection of UCUM unit strings
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public class UCUM {
  public static final String kg = "kg";
  // TODO: The unit should be mm[Hg] as this is the proper UCUM unit string...  
  public static final String mmHg = "mmHg";
  public static final String gPerL = "g/L";
  public static final String pct = "%";
  // TODO: Validate that liter is indeed 'L';
  public static final String L = "L";
}
