package org.net4care.phmr.codes;

/** Null flavors in HL7
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public enum NullFlavor {
  NO_INFORMATION
}
