package org.net4care.phmr.model;

import java.util.*;

import org.net4care.phmr.model.AddressData.*;

/** Immutable object for information of an organization.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */

public final class OrganizationIdentity implements Locateable {

  private String orgOID;
  private String orgName;
  private Telecom[] telecomArray;
  private AddressData address;

  /** "Effective Java" builder for constructing
   * organization objects.
   * 
   * @author Henrik Baerbak Christensen, Aarhus University
   *
   */
  public static class OrganizationBuilder {

    // Required fields
    private String orgOID;
    // Optional fields ?
    private AddressData address = null;
    private String orgName = null;
    private Telecom []telecom = null;

    // Temporaries
    private List<Telecom> teleComListTemporary;

    public OrganizationBuilder(String oid) {
      this.orgOID = oid;
      teleComListTemporary = new ArrayList<Telecom>();
    }

    public OrganizationBuilder setAddress(AddressData addr) {
      this.address = addr;
      return this;
    }

    public OrganizationBuilder setName(String orgName) {
      this.orgName = orgName;
      return this;
    }

    public OrganizationBuilder addTelecom(Use use, String protocol, String telecom) {
      teleComListTemporary.add(new Telecom(use, protocol, telecom));
      return this;
    }

    public OrganizationIdentity build() {
      OrganizationIdentity org;
      
      if (teleComListTemporary.size() > 0) {
        telecom = new Telecom[ teleComListTemporary.size()];
        teleComListTemporary.toArray(telecom);
      }  else {
          telecom = new Telecom[] {};
      }

      org = new OrganizationIdentity(this);
      return org;
    }
  }

  private OrganizationIdentity(OrganizationBuilder builder) {
    orgOID = builder.orgOID;
    orgName = builder.orgName;
    address = builder.address;
    telecomArray = builder.telecom;
  }

  /** Get the organization OID.
   * @return The organization OID.
   */
  public String getOrgOID() {
    return orgOID;
  }

  /** Get the organization name.
   * @return The organization name.
   */
  public String getOrgName() {
    return orgName;
  }

  @Override
  public Telecom[] getTelecomList() {
    return telecomArray;
  }

  @Override
  public AddressData getAddress() {
    return address;
  }
  
  @Override
  public String toString() {
    String result = "Org: " + orgOID + " ";
    
    result += " / " + orgName + " ";
    
    result += "Adr: " + address.toString() + " ";
    
    if (telecomArray != null) {
      result += "Telecom: [";
      for (Telecom tc : getTelecomList()) {
        result += tc + ",";
      }
      result += "] ";
    }

    return result;
  }
}
