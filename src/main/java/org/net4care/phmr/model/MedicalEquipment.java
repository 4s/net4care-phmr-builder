package org.net4care.phmr.model;

/** Medical equipment class. * 
 */
public final class MedicalEquipment {
    private String medicalDeviceCode;
    private String medicalDeviceDisplayName;
    private String manufacturerModelName;
    private String softwareName;
    
    /** The Builder for creating instances of a measurement.*/
    public static class MedicalEquipmentBuilder {
      
      private String medicalDeviceCode = null;
      private String medicalDeviceDisplayName = null;
      private String manufacturerModelName = null;
      private String softwareName = null;

      
      /** Build the measurement based upon the given parameters.
       * 
       * @return the final measurement.
       */
      public MedicalEquipment build() {
        validateWellformednessAndThrowExceptionIfNot();
        return new MedicalEquipment(medicalDeviceCode, 
            medicalDeviceDisplayName, manufacturerModelName, softwareName, false);
      }

      public void validateWellformednessAndThrowExceptionIfNot() {
        if ( medicalDeviceCode == null ) {
          throw new RuntimeException("The MedicalEquipment's device code is null");
        }
        if ( medicalDeviceDisplayName == null ) {
          throw new RuntimeException("The MedicalEquipment's device displayname is null");
        }
        if ( manufacturerModelName == null ) {
          throw new RuntimeException("The MedicalEquipment's manufacturer model name is null");
        }
        if ( softwareName == null ) {
          throw new RuntimeException("The MedicalEquipment's software name is null");
        }       
      }

      public MedicalEquipmentBuilder setMedicalDeviceCode(String theDeviceCode) {
        medicalDeviceCode = theDeviceCode;
        return this;
      }
      public MedicalEquipmentBuilder setMedicalDeviceDisplayName(String theDisplayName) {
        medicalDeviceDisplayName = theDisplayName;
        return this;
      }
      public MedicalEquipmentBuilder setManufacturerModelName(String theModelName) {
        manufacturerModelName = theModelName;
        return this;
      }
      public MedicalEquipmentBuilder setSoftwareName(String theSoftwareName) {
        softwareName = theSoftwareName;
        return this;
      }

    }

    @Deprecated
    public MedicalEquipment(String deviceCode, String deviceDisplayName, String modelName, String softwareName) {
      this(deviceCode, deviceDisplayName, modelName, softwareName, false);
    }
    /** Medical equipment constructor.
     * 
     * @param deviceCode The device code.
     * @param deviceDisplayName The device display name.
     * @param modelName The model name.
     * @param softwareName The software name.
     */
    private MedicalEquipment(String deviceCode, String deviceDisplayName, String modelName, String softwareName, boolean dummy) {
      this.medicalDeviceCode = deviceCode;
      this.medicalDeviceDisplayName = deviceDisplayName;
      this.manufacturerModelName = modelName;
      this.softwareName = softwareName;
    }


    /** Get the medical device code.
     * @return The medical device code.
     */
    public String getMedicalDeviceCode() {
      return medicalDeviceCode;
    }

    /** Get the medical device display name.
     * @return The medical device display name.
     */
    public String getMedicalDeviceDisplayName() {
      return medicalDeviceDisplayName;
    }

    /** Get the manufacturer model name.
     * @return The manufacturer model name.
     */
    public String getManufacturerModelName() {
      return manufacturerModelName;
    }

    /** Get the software name.
     * @return The software name.
     */
    public String getSoftwareName() {
      return softwareName;
    }
    
    /** Return string representation of this
     * instance.
     * @return the string representation
     */
    public String toString() {
      String value;
      value = "Medical Equipment: " + getMedicalDeviceCode() 
          + ", " + getMedicalDeviceDisplayName()
          + ", " + getManufacturerModelName()
          + ", " + getSoftwareName();
      return value;
    }
}
