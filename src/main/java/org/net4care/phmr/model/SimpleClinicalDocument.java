package org.net4care.phmr.model;

import org.net4care.core.CoreClinicalDocument;


/** SimpleClinicalDocument is inspired by the GreenCDA project that
 * focus on 'dynamic fields' (user data) while avoiding 'static fields'
 * (boilerplate data, like template ids, typeCodes, etc) of the CDA.
 * 
 * A SimpleClinicalDocument is a Java data object
 * that contains the business related data for telemedicine, like
 * patient information, measurements, device information, custodian
 * organization, etc.
 * 
 * To produce valid PersonalHealthMonitoringRecord XML documents
 * you use the 'construct' method with a suitable PHMRBuilder instance.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public interface SimpleClinicalDocument extends CoreClinicalDocument {

  /** Add the medical equipment used for making the measurements.
   * 
   *  @param equipment The medical equipment.
   *  */
  void addMedicalEquipment(MedicalEquipment equipment);

  /** Add a result measurement. 
   * 
   * @param measurement The result measurement to add.
   */
  void addResult(Measurement measurement);

  /** Add a vital sign measurement. 
   * 
   * @param measurement The vital sign measurement to add.
   */
  void addVitalSign(Measurement measurement);
}
