package org.net4care.phmr.model;

import java.util.*;

import org.net4care.phmr.codes.NullFlavor;
import org.net4care.phmr.model.AddressData.Use;

/** Immutable object to contain information about a person.
 *
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public final class PersonIdentity implements Locateable {

  /** Gender enumeration. */
  public enum Gender {
    /** Female gender. */
    Female,
    /** Male gender. */
    Male,
    /** Undifferentiated gender. */
    Undifferentiated
  }

  private String ssn;
  private String prefix;
  private String[] givenNames;
  private String familyName;
  private Gender gender;
  private Date birthTime;

  private AddressData address;

  private Telecom[] telecomArray;
  
  // If nullFlavor is not null, then
  // this information takes precedens over
  // all other fields which are probably null.
  private NullFlavor nullFlavor;


  /** "Effective Java" builder for constructing person identities.
   *
   * @author Henrik Baerbak Christensen, Aarhus University
   *
   */
  public static class PersonBuilder {
    // Required parameters
    private String familyName;
    private String[] givenNames;

    // Optional parameters
    private String patientId = null;
    private Gender gender = Gender.Undifferentiated;
    private Date birthTime = null;
    private Telecom[] telecom = null;
    private String prefix = null;
    
    private NullFlavor nullFlavor = null;

    // temporaries
    private int yearAllDigits = 0;
    private int dayInMonth = 0;
    private int month = 0;
    private List<String> givenNamesTemporary;
    private List<Telecom> teleComListTemporary;
    private AddressData address;

    public PersonBuilder() {
      givenNamesTemporary = new ArrayList<String>();
      teleComListTemporary = new ArrayList<Telecom>();
    }

    public PersonBuilder(String familyName) {
      givenNamesTemporary = new ArrayList<String>();
      teleComListTemporary = new ArrayList<Telecom>();

      this.familyName = familyName;
    }
    public PersonBuilder addFamilyName(String familyName) {
      this.familyName = familyName;
      return this;
    }

    public PersonBuilder addGivenName(String name) {
      givenNamesTemporary.add(name);
      return this;
    }
    public PersonBuilder setPersonID(String patientId) {
      this.patientId = patientId;
      return this;
    }
    public PersonBuilder setGender(Gender gender) {
      this.gender = gender;
      return this;
    }
    public PersonBuilder setPrefix(String prefix) {
      this.prefix = prefix;
      return this;
    }
    public PersonBuilder setBirthTime(int yearAllDigits, int monthUsingCalendarEnums, int dayInMonth) {
      this.yearAllDigits = yearAllDigits;
      this.month = monthUsingCalendarEnums;
      this.dayInMonth = dayInMonth;
      return this;
    }
    public PersonBuilder addTelecom(Use use, String protocol, String telecomString) {
      teleComListTemporary.add(new Telecom(use, protocol, telecomString));
      return this;
    }
    public PersonBuilder setAddress(AddressData address) {
      this.address = address;
      return this;
    }
    // This is an alternative to the rest of the setters */
    public PersonBuilder NoInformation() {
      this.nullFlavor = NullFlavor.NO_INFORMATION;
      return this;
    }

    public PersonIdentity build() {
      givenNames = new String[givenNamesTemporary.size()];
      givenNamesTemporary.toArray(givenNames);

      if (teleComListTemporary.size() > 0) {
        telecom = new Telecom[ teleComListTemporary.size()];
        teleComListTemporary.toArray(telecom);
      }

      if (yearAllDigits != 0) {
        Calendar utcCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        utcCalendar.set(yearAllDigits, month, dayInMonth, 0, 0, 0);
        birthTime = utcCalendar.getTime();
      }
      PersonIdentity pi = new PersonIdentity(this);
      return pi;
    }
  }

  private PersonIdentity(PersonBuilder builder) {
    // Clumsy logic, but "when null flavor is null, then patient data exists"
    if (builder.nullFlavor == null) {
      familyName = builder.familyName;
      givenNames = builder.givenNames;
      ssn = builder.patientId;
      gender = builder.gender;
      birthTime = builder.birthTime;
      telecomArray = builder.telecom;
      prefix = builder.prefix;
      address = builder.address;
      
      nullFlavor = null;
    } else {
      familyName = null;
      givenNames = null;
      ssn = null;
      gender = null;
      birthTime = null;
      telecomArray = null;
      prefix = null;
      address = null;
      
      nullFlavor = builder.nullFlavor; 
    }
  }


  /** Get the social security number.
   * @return The social security number.
   */
  public String getSSN() {
    return ssn;
  }

  /** Get whether person has a prefix.
   * @return Boolean stating whether there is a prefix.
   */
  public boolean hasPrefix() {
    return prefix != null && prefix.length() > 0;
  }

  /** Get person prefix.
   * @return prefix string.
   */
  public String getPrefix() {
    return prefix;
  }

  /** Get list of given names.
   * @return list of given names.
   */
  public String[] getGivenNames() {
    return givenNames;
  }

  /** Get family name.
   * @return family name string.
   */
  public String getFamilyName() {
    return familyName;
  }

  /** Get person gender.
   * @return gender type.
   */
  public Gender getGender() {
    return gender;
  }

  /** Get person birth time.
   * @return birth time date.
   */
  public Date getBirthTime() {
    return birthTime;
  }

  /** Get person address.
   * @return address data.
   */
  @Override
  public AddressData getAddress() {
    return address;
  }

  /** Get person telecom.
   * @return telecom list.
   */
  @Override
  public Telecom[] getTelecomList() {
    if (telecomArray == null) {
      telecomArray = new Telecom[]{}; // Empty array
    }


    return telecomArray;
  }

  /** Return the null flavor if this person
   * identity represents some kind of null value.
   * If the person identity is known and defined
   * this method returns null.
   * @return the null flavor or null in case the
   * person is well defined
   */
  public NullFlavor getNullFlavor() {
    return nullFlavor;
  }

  public String toString() {
    String result = "PersonIdentity: ";
    // Clumsy logic, but "when null flavor is null, then patient data exists"
    if ( nullFlavor == null ) {

      result += "G: " + getGender() + " ";

      result += "ID: " + getSSN() + " ";

      result += "(" + getFamilyName() + ",[";
      for (String firstname: getGivenNames()) {
        result += firstname + "/";
      }
      result += "] ";

      result += "Prefix: " + prefix + " ";

      result += "Birth: " + getBirthTime() + " ";

      if ( address != null ) {
        result += "Adr: " + address.toString() + " ";
      } else {
        result += "Adr: (null)";
      }

      if (telecomArray != null) {
        result += "Telecom: [";
        for (Telecom tc : getTelecomList()) {
          result += tc + ",";
        }
        result += "] ";
      }

      result += ")";
    } else {
      result += "NullFlavor: "+ nullFlavor +"\n";
    }
    return result;
  }

}
