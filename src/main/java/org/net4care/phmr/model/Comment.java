package org.net4care.phmr.model;

import java.util.Date;

/** 
 * 
 *
 */
public final class Comment {
  private PersonIdentity author;
  private OrganizationIdentity organization;
  private String text;
  private Date time;

  /** Comment class constructor.
   * 
   * @param author The author person identity.
   * @param organization The organization identity.
   * @param time The time for the comment.
   * @param text The comment text.
   */
  public Comment(PersonIdentity author, OrganizationIdentity organization, Date time, String text) {
    this.author = author;
    this.organization = organization;
    this.time = time;
    this.text = text;
  }

  /** Get author person identity.
   * @return author person identity.
   */
  public PersonIdentity getAuthor() {
    return author;
  }

  /** Get organization identity.
   * @return organization identity.
   */
  public OrganizationIdentity getOrganization() {
    return organization;
  }

  /** Get time of comment.
   * @return time of comment.
   */
  public Date getTime() {
    return time;
  }

  /** Get comment text. 
   * @return comment text.
   */
  public String getText() {
    return text;
  }
}
