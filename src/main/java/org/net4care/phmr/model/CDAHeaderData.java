package org.net4care.phmr.model;

import java.util.Date;

/** Data object storing all CDA header data
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public class CDAHeaderData {
  
  private PersonIdentity patientIdentity;
  private PersonIdentity authorIdentity;
  private OrganizationIdentity authorOrganizationIdentity;
  private Date effectiveTime;
  private Date authorParticipationStartTime;
  private OrganizationIdentity custodianIdentity;
  private OrganizationIdentity authenticatorOrganizationIdentity;
  private PersonIdentity authenticatorPersonIdentity;
  private Date timeOfAutentication;
  private Date serviceEnd;
  private Date serviceStart;
  private String setId;
  private String versionNumber;
  
  private String title;
  private String templateId;

  public CDAHeaderData() {
  }

  public PersonIdentity getPatientIdentity() {
    return patientIdentity;
  }

  public void setPatientIdentity(PersonIdentity patientIdentity) {
    this.patientIdentity = patientIdentity;
  }

  public PersonIdentity getAuthorIdentity() {
    return authorIdentity;
  }

  public void setAuthorIdentity(PersonIdentity authorIdentity) {
    this.authorIdentity = authorIdentity;
  }

  public OrganizationIdentity getAuthorOrganizationIdentity() {
    return authorOrganizationIdentity;
  }

  public void setAuthorOrganizationIdentity(
      OrganizationIdentity authorOrganizationIdentity) {
    this.authorOrganizationIdentity = authorOrganizationIdentity;
  }

  public Date getEffectiveTime() {
    return effectiveTime;
  }

  public void setEffectiveTime(Date effectiveTime) {
    this.effectiveTime = effectiveTime;
  }

  public Date getAuthorParticipationStartTime() {
    return authorParticipationStartTime;
  }

  public void setAuthorParticipationStartTime(Date authorParticipationStartTime) {
    this.authorParticipationStartTime = authorParticipationStartTime;
  }

  public OrganizationIdentity getCustodianIdentity() {
    return custodianIdentity;
  }

  public void setCustodianIdentity(OrganizationIdentity custodianIdentity) {
    this.custodianIdentity = custodianIdentity;
  }

  public OrganizationIdentity getAuthenticatorOrganizationIdentity() {
    return authenticatorOrganizationIdentity;
  }

  public void setAuthenticatorOrganizationIdentity(
      OrganizationIdentity authenticatorOrganizationIdentity) {
    this.authenticatorOrganizationIdentity = authenticatorOrganizationIdentity;
  }

  public PersonIdentity getAuthenticatorPersonIdentity() {
    return authenticatorPersonIdentity;
  }

  public void setAuthenticatorPersonIdentity(
      PersonIdentity authenticatorPersonIdentity) {
    this.authenticatorPersonIdentity = authenticatorPersonIdentity;
  }

  public Date getTimeOfAutentication() {
    return timeOfAutentication;
  }

  public void setTimeOfAutentication(Date timeOfAutentication) {
    this.timeOfAutentication = timeOfAutentication;
  }

  public Date getServiceEnd() {
    return serviceEnd;
  }

  public void setServiceEnd(Date serviceEnd) {
    this.serviceEnd = serviceEnd;
  }

  public Date getServiceStart() {
    return serviceStart;
  }

  public void setServiceStart(Date serviceStart) {
    this.serviceStart = serviceStart;
  }

  public String getSetId() {
    return setId;
  }

  public void setSetId(String setId) {
    this.setId = setId;
  }

  public String getVersionNumber() {
    return versionNumber;
  }

  public void setVersionNumber(String versionNumber) {
    this.versionNumber = versionNumber;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  // Root codes
  public String getTemplateId() {
    return templateId;
  }

  public void setTemplateId(String templateId) {
    this.templateId = templateId;
  }

  private String codeCode;
  private String codeDisplayName;

  public String getCodeCode() {
    return codeCode;
  }

  public void setCodeCode(String codeCode) {
    this.codeCode = codeCode;
  }

  public String getCodeDisplayName() {
    return codeDisplayName;
  }

  public void setCodeDisplayName(String codeDisplayName) {
    this.codeDisplayName = codeDisplayName;
  }
  

}