package org.net4care.phmr.model;

import java.util.*;

public class Observation {

  private Map<String,Object> contents;
  
  public Observation() {
    contents = new HashMap<String,Object>();
  }
  
  public void put(String key, Object anItem) {
    // System.out.println(" Storing under key "+ key+ " : "+anItem);
    contents.put(key, anItem);
  }

  public Map<String, Object> getContents() {
    return contents;
  }

  @Override
  public String toString() {
    return "Observation [contents=" + contents + "]";
  }

}
