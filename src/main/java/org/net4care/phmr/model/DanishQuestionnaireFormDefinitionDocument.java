package org.net4care.phmr.model;

import java.util.*;

import org.net4care.bro.QFDSection;
import org.net4care.core.*;
import org.net4care.phmr.codes.*;

public class DanishQuestionnaireFormDefinitionDocument implements
    CoreClinicalDocument {

  private CDAHeaderData data;
  private String copyrightText;

  // TODO: move up in abstract super class!
  private List<Section> sectionList;

  public DanishQuestionnaireFormDefinitionDocument() {
    super();
    
    data = new CDAHeaderData();
    data.setTitle("QFD");
    data.setTemplateId(MedCom.DK_QFD_ROOT_OID);
    data.setCodeCode(Loinc.QFD_CODE);
    data.setCodeDisplayName(Loinc.QFD_DISPLAYNAME);
    
    sectionList = new ArrayList<Section>(10);

  }

  public void setTitle(String title) {
    data.setTitle(title);
  }
  public void setEffectiveTime(Date documentCreationTime) {
    data.setEffectiveTime(documentCreationTime);
  }


  public void setDocumentVersion(String uniqueId, int versionNumber) {
    data.setSetId(uniqueId);
    data.setVersionNumber("" + versionNumber);
  }


  public void setPatient(PersonIdentity patientIdentity) {
    data.setPatientIdentity(patientIdentity);
  }


  public void setAuthor(OrganizationIdentity authorOrganizationIdentity,
      PersonIdentity authorPersonIdentity, Date authorParticipationStart) {
    data.setAuthorOrganizationIdentity(authorOrganizationIdentity);
    data.setAuthorIdentity(authorPersonIdentity);
    data.setAuthorParticipationStartTime(authorParticipationStart);
  }


  public void setCustodian(OrganizationIdentity custodianIdentity) {
    data.setCustodianIdentity(custodianIdentity);
  }


  public void setAuthenticator(
      OrganizationIdentity authenticatorOrganizationIdentity,
      PersonIdentity authenticatorPersonIdentity, Date timeOfAuthentication) {
    throw new RuntimeException("Legal authenticator is not used in QFD documents.");
  }


  public void setDocumentationTimeInterval(Date from, Date to) {
    data.setServiceStart(from);
    data.setServiceEnd(to);
  }

  @Override
  public void construct(ClinicalDocumentBuilder builder) {
    //TODO: Duplicated code here that needs to be encapsulated and reused
    builder.buildRootNode();
    // === CDA Header
    builder.buildHeader(data);
    builder.buildContext(data.getTitle(), data.getPatientIdentity(), data.getEffectiveTime(), 
        data.getSetId(), data.getVersionNumber());
    builder.buildPatientSection(data.getPatientIdentity());
    builder.buildAuthorSection(data.getAuthorOrganizationIdentity(), data.getAuthorIdentity(), 
        data.getAuthorParticipationStartTime());
    builder.buildCustodianSection(data.getCustodianIdentity());
    builder.buildLegalAuthenticatorSection(data.getAuthenticatorOrganizationIdentity(), 
        data.getAuthenticatorPersonIdentity(), data.getTimeOfAutentication());
    builder.buildDocumentationOf(data.getServiceStart(), data.getServiceEnd());
    
    // CDA Body
    builder.buildStructuredBodySection();
    
    builder.buildSections(sectionList);

    builder.buildCopyrightSection(copyrightText);
  }

  public void addQFDSection(QFDSection section) {
    Section sec = new Section();
    sectionList.add(sec);
    org.net4care.core.Entry newEntry = sec.createEntry();
    
    Observation obs = newEntry.createObservation();
    obs.put("QFDSection", section);
  }

  public void addCopyright(String copyrightText) {
    this.copyrightText = copyrightText;
  }

}
