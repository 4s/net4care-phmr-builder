/** Package for the classes that defines the model of a clinical document
 * in the 'GreenCDA' spirit. That is, an object model focusing on
 * modeling the dynamic aspects of a CDA (person names, addresses, organizations,
 * stewards, measurements, medical equipment, etc.), while static aspects
 * (proper codes OIDs for (Danish) PHMR, CDA tag names, etc.) are left to
 * the Builders.
 */
package org.net4care.phmr.model;
