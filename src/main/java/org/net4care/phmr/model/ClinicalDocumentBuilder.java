package org.net4care.phmr.model;

import java.util.Date;
import java.util.List;

import org.net4care.core.Section;

/** Builder interface for building representations of a SimpleClinicalDocument.
 *
 * Used by the 'construct' method of a SimpleClinicalDocument.
 *
 * @author Henrik Baerbak Christensen, Aarhus University
 * @author Lars Christian Duus Hausmann, Silverbullet A/S
 */

public interface ClinicalDocumentBuilder {

    // === CDA Header

    /** Build the root node.
     */
    void buildRootNode();

    /** Build the header.
     */
    void buildHeader(CDAHeaderData data);

    /** Build the context section.
     *
     * @param title the document title
     * @param patientIdentity The person identity.
     * @param effectiveTime The effective time.
     * @param setId The set identifier.
     * @param versionNumber The version number.
     */
    void buildContext(String title,
        PersonIdentity patientIdentity, Date effectiveTime, 
        String setId, String versionNumber);

    /** Build the patient section.
     * @param patientIdentity The patient identity.
     */
    void buildPatientSection(PersonIdentity patientIdentity);

    /** Build author section.
     *
     * @param authorOrganizationIdentity The author organization identity.
     * @param authorIdentity The author person identity
     * @param authorParticipationStartTime The author participation start time.
     */
    void buildAuthorSection(
            OrganizationIdentity authorOrganizationIdentity,
            PersonIdentity authorIdentity,
            Date authorParticipationStartTime);

    /** Build custodian section.
     *
     * @param custodianIdentity The custodian person identity.
     */
    void buildCustodianSection(OrganizationIdentity custodianIdentity);

    /** Build legal authenticator section.
     *
     * @param authenticatorOrganizationIdentity The authenticator organization identity.
     * @param authenticatorPersonIdentity The authenticator person identity.
     * @param timeOfAuthentication The time of authentication.
     */
    void buildLegalAuthenticatorSection(
            OrganizationIdentity authenticatorOrganizationIdentity,
            PersonIdentity authenticatorPersonIdentity,
            Date timeOfAuthentication);

    /** Build documentation section.
     *
     * @param serviceStart The time of service start.
     * @param serviceEnd The time of service end.
     */
    void buildDocumentationOf(Date serviceStart, Date serviceEnd);

    // === CDA Body
    /** Build structured body section.
     */
    void buildStructuredBodySection();
    
  /**
   * Build the sections in the structured body
   * 
   * @param sectionList
   */
    void buildSections(List<Section> sectionList);

    // TODO "Make horrible sins and then clean up" - separate QFD and PHMR code
    @Deprecated
    void buildCopyrightSection(String copyrightText);

}
