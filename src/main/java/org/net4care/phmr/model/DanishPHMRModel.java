package org.net4care.phmr.model;

import java.util.*;

import org.net4care.core.Section;
import org.net4care.phmr.codes.*;

/** An implementation of SimpleClinicalDocument that
 * only supports the data of the Danish PHMR.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */

public final class DanishPHMRModel implements SimpleClinicalDocument {

  private CDAHeaderData data = new CDAHeaderData();


  // TODO: move up in abstract super class!
  private List<Section> sectionList;

  /** Danish PHMR model constructor.
   */
  public DanishPHMRModel() {
    data.setTitle("Hjemmemonitorering for (ukendt)");
    data.setTemplateId(MedCom.DK_PHMR_ROOT_POD);
    data.setCodeCode(Loinc.PHMR_CODE);
    data.setCodeDisplayName(Loinc.PMHR_DISPLAYNAME);
    
    sectionList = new ArrayList<Section>(3);
    sectionList.add( new Section() ); // Vital signs
    sectionList.add( new Section() ); // Results
    sectionList.add( new Section() ); // Medical Equipment
  }

  @Override
  public void setDocumentVersion(String uniqueId, int versionNumber) {
    data.setSetId(uniqueId);
    data.setVersionNumber("" + versionNumber);
  }

  /* (non-Javadoc)
   * @see org.net4care.phmr.test.ClinicalDocument#setPatientRole(org.net4care.phmr.test.model.PersonIdentity)
   */
  @Override
  public void setPatient(PersonIdentity patientIdentity) {
    data.setPatientIdentity(patientIdentity);
    if ( patientIdentity != null && patientIdentity.getNullFlavor() == null ) {
      data.setTitle("Hjemmemonitorering for "+patientIdentity.getSSN());
    }
  }

  /* (non-Javadoc)
   * @see org.net4care.phmr.test.ClinicalDocument#construct(org.net4care.phmr.test.Builder)
   */
  @Override
  public void construct(ClinicalDocumentBuilder builder) {
    builder.buildRootNode();
    // === CDA Header
    builder.buildHeader(data);
    builder.buildContext(data.getTitle(), data.getPatientIdentity(), data.getEffectiveTime(), data.getSetId(), data.getVersionNumber());
    builder.buildPatientSection(data.getPatientIdentity());
    builder.buildAuthorSection(data.getAuthorOrganizationIdentity(), data.getAuthorIdentity(), data.getAuthorParticipationStartTime());
    builder.buildCustodianSection(data.getCustodianIdentity());
    builder.buildLegalAuthenticatorSection(data.getAuthenticatorOrganizationIdentity(), 
        data.getAuthenticatorPersonIdentity(), data.getTimeOfAutentication());
    builder.buildDocumentationOf(data.getServiceStart(), data.getServiceEnd());
    
    // CDA Body
    builder.buildStructuredBodySection();

    // Results section
    // BRO 2.1
    builder.buildSections(sectionList);
  }

  /* (non-Javadoc)
   * @see org.net4care.phmr.test.ClinicalDocument#setDocumentationTime(java.util.Date, java.util.Date)
   */
  @Override
  public void setDocumentationTimeInterval(Date from, Date to) {
    data.setServiceStart(from);
    data.setServiceEnd(to);
  }

  /* (non-Javadoc)
   * @see org.net4care.phmr.test.ClinicalDocument#setEffectiveTime(java.util.Date)
   */
  @Override
  public void setEffectiveTime(Date now) {
    data.setEffectiveTime(now);
  }

  /* (non-Javadoc)
   * @see org.net4care.phmr.test.ClinicalDocument#setAuthor(org.net4care.phmr.test.model.PersonIdentity, java.util.Date)
   */
  @Override
  public void setAuthor(OrganizationIdentity authorOrganizationIdentity,
      PersonIdentity authorIdentity, Date authorParticipationStart) {
    data.setAuthorOrganizationIdentity(authorOrganizationIdentity);
    data.setAuthorIdentity(authorIdentity);
    data.setAuthorParticipationStartTime(authorParticipationStart);
  }

  /* (non-Javadoc)
   * @see org.net4care.phmr.test.ClinicalDocument#setCustodian(org.net4care.phmr.test.model.OrganizationIdentity)
   */
  @Override
  public void setCustodian(OrganizationIdentity custodianIdentity) {
    data.setCustodianIdentity(custodianIdentity);
  }

  /* (non-Javadoc)
   * @see org.net4care.phmr.test.ClinicalDocument#setAuthenticator(org.net4care.phmr.test.model.PersonIdentity)
   */
  @Override
  public void setAuthenticator(OrganizationIdentity authenticatorOrganizationIdentity,
        PersonIdentity authenticatorPersonIdentity, Date timeOfAuthentication) {
    data.setAuthenticatorOrganizationIdentity(authenticatorOrganizationIdentity);
    data.setAuthenticatorPersonIdentity(authenticatorPersonIdentity);
    data.setTimeOfAutentication(timeOfAuthentication);
  }

  
  @Override
  public void addResult(Measurement aMeasurement) {
    
    // resultList.add(aMeasurement);
    
    // Bro 2.1
    Section sec = sectionList.get(1); // result section is 2. in the list
    org.net4care.core.Entry newEntry = sec.createEntry();
    
    Observation obs = newEntry.createObservation();
    obs.put("measurement", aMeasurement);
    
  }

  @Override
  public void addVitalSign(Measurement aMeasurement) {
    // vitalSignList.add(aMeasurement);

    // Bro 2.1
    Section sec = sectionList.get(0); // result section is 1. in the list
    org.net4care.core.Entry newEntry = sec.createEntry();
    
    Observation obs = newEntry.createObservation();
    obs.put("measurement", aMeasurement);
  }

  @Override
  public void addMedicalEquipment(MedicalEquipment equipment) {
    // equipmentList.add(equipment);

    // Bro 2.1
    Section sec = sectionList.get(2); // result section is 3. in the list
    org.net4care.core.Entry newEntry = sec.createEntry();
    
    Observation obs = newEntry.createObservation();
    obs.put("equipment", equipment);
  }
}
