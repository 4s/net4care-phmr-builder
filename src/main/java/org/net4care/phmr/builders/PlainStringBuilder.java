package org.net4care.phmr.builders;

import java.util.Date;
import java.util.List;

import org.net4care.core.Section;
import org.net4care.phmr.model.*;

/** A builder that just constructs a plain string representation
 * of a SimpleClinicalDocument. Used by the toString() method
 * of the CDA.
 *
 * Still work in progress...
 *
 * @author Henrik Baerbak Christensen, Aarhus University
 * @author Lars Chr. Duus Hausmann, Silverbullet A/S
 *
 */
public final class PlainStringBuilder implements ClinicalDocumentBuilder {

  private String result;

  /** Get the plain string result.
   *
   * @return The plain string result.
   */
  public String getResult() {
    return result;
  }

  @Override
  public void buildRootNode() {
    result = "SimpleClinicalDocument:\n";
  }

  @Override
  public void buildHeader(CDAHeaderData data) {
  }

  @Override
  public void buildContext(String title, PersonIdentity patientIdentity, Date effectiveTime,
                           String setId, String versionNumber) {
    result += " setId: " + setId + " / version: " + versionNumber + "\n";
  }

  @Override
  public void buildPatientSection(PersonIdentity patientIdentity) {
    // result += patientIdentity.toString()+"\n";
    result += " Patient: " + patientIdentity.getSSN() + ": " + patientIdentity.getGivenNames()[0] + " " + patientIdentity.getFamilyName() + "\n";
  }

  private int count = 0;

  public void buildGenericMeasurement(Measurement m) {
    result += "    " + count + ": " + m.getTimestamp() + " :: ";
    Measurement num = (Measurement) m;
    if (num.getType() == Measurement.Type.PHYSICAL_QUANTITY) {
      Measurement n = (Measurement) m;
      result += n.getDisplayName() + " " + n.getValue() + " " + n.getUnit();
    } else if (num.getType() == Measurement.Type.OBSERVATION_MEDIA) {
      result += num.getReference();
    }
    result += "\n";
    count++;
  }


  @Override
  public void buildStructuredBodySection() {

  }


  @Override
  public void buildAuthorSection(
      OrganizationIdentity authorOrganizationIdentity,
      PersonIdentity authorIdentity, Date authorParticipationStartTime) {
    result += " Author: \n";
    result += "   " + authorOrganizationIdentity.getOrgName() + "\n";
    result += "    - " + authorIdentity.getGivenNames()[0] + " " + authorIdentity.getFamilyName() + "\n";
  }

  @Override
  public void buildCustodianSection(OrganizationIdentity custodianIdentity) {
    result += " Custodian: \n";
    result += "   " + custodianIdentity.getOrgName() + "\n";

  }

  @Override
  public void buildLegalAuthenticatorSection(
      OrganizationIdentity authenticatorOrganizationIdentity,
      PersonIdentity authenticatorPersonIdentity, Date timeOfAuthentication) {
    result += " Authenticator: \n";
    result += "   " + authenticatorOrganizationIdentity.getOrgName() + "\n";
    result += "    - " + authenticatorPersonIdentity.getGivenNames()[0] + " " + authenticatorPersonIdentity.getFamilyName() + "\n";
  }

  @Override
  public void buildDocumentationOf(Date serviceStart, Date serviceEnd) {
  }

  @Override
  public void buildCopyrightSection(String text) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void buildSections(List<Section> sectionList) {
    // TODO Auto-generated method stub
    
  }
}
