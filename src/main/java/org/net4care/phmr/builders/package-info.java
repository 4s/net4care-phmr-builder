/** Package for builders used by the SimpleClinicalDocument to
 * build various representations, notably PHMR, of the document.
 * 
 */
package org.net4care.phmr.builders;
