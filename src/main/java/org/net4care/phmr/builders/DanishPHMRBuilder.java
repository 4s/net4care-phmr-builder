package org.net4care.phmr.builders;

import java.text.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.xml.bind.*;

import org.hl7.v3.*;
import org.net4care.core.*;
import org.net4care.phmr.codes.HL7;
import org.net4care.phmr.codes.Loinc;
import org.net4care.phmr.codes.MedCom;
import org.net4care.phmr.codes.NSI;
import org.net4care.phmr.model.*;
import org.net4care.phmr.model.Measurement.Status;
import org.w3c.dom.Element;

/**
 * An implementation of the ClinicalDocumentBuilder that can build
 * an XML Document object following the Danish PHMR standard.
 *
 * To use you should:
 *
 * a) Create a SimpleClinicalDocument instance, i
 * b) Instantiate an instance of this class, b
 * c) invoke i.construct(b);
 * d) the document can be retrieved using b.getDocument()
 * 
 * Note that there are several 'get' methods for getting
 * the document as String or as the actual POCDMT... clinical document.
 *
 * Implementation note: JAXB classes from connect open source is used for
 * JAXB binding
 *
 * @author Henrik Baerbak Christensen, Aarhus University
 * @author Lars Christian Duus Hausmann, Silverbullet A/S
 */

public class DanishPHMRBuilder extends BaseClinicalDocumentBuilder implements ClinicalDocumentBuilder {
  
  public DanishPHMRBuilder(UUIDStrategy stubUUIDStrategy) {
    super(stubUUIDStrategy);
  }

  public DanishPHMRBuilder() {
    super();
  }


  /**
   * Creates en section containing vitalsigns and results
   * @param templateIds The template ids to be used
   * @param ce The code system to be used
   * @param stringTitle The title in the Title the section
   * @param measurements  The measurements to be presented
   * @return
   */
  private POCDMT000040Component3 createGenericMeasurements(List<String> templateIds, CE ce, String stringTitle, List<Measurement> measurements) {
//        ArrayList<LaboratoryReport> vitalSigns
    POCDMT000040Component3 retVal = new POCDMT000040Component3();
    retVal.setTypeCode(ActRelationshipHasComponent.COMP);
    retVal.setContextConductionInd(Boolean.TRUE);
//
    POCDMT000040Section section = new POCDMT000040Section();
    section.getClassCode().add("DOCSECT");
    section.getMoodCode().add("EVN");
//
    for (String id : templateIds) {
      II ii = new II();
      ii.setRoot(id);
      section.getTemplateId().add(ii);
    }
//
    section.setCode(ce);

    STExplicit title = new STExplicit();
    title.getContent().add(stringTitle);
    section.setTitle(title);
//
    Element textElement = docContextForCreatingTextSections.createElement("text");
    textElement.setAttribute("xmlns", "urn:hl7-org:v3");
    section.setText(textElement);

    SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    if (measurements.size() > 0) {
      StringBuffer sb = new StringBuffer();

      for (Measurement measurement: measurements) {
        String value = "";
        if (measurement.getValue() != null) {
          value = measurement.getValue();
        }

        String unit = "";
        if (measurement.getUnit() != null) {
          unit = measurement.getUnit();
        }

        String timeStamp = "";
        if (measurement.getUnit() != null) {
          timeStamp = ft.format(measurement.getTimestamp());
        }

        String context = "";
        if (measurement.getContext() != null) {
          context = PerformerMethodDisplayName(measurement) + ", " + ProvisionMethodDisplayName(measurement);
        }

        String displayName = "";
        if (measurement.getDisplayName() != null) {
          displayName = measurement.getDisplayName();
        }

        sb.append(timeStamp).append(", ")
          .append(value).append(" ")
          .append(unit).append(" ")
          .append(displayName).append(", ")
          .append(context).append("\n");
      }

      // TODO: encode this for XML?
      textElement.setTextContent(sb.toString());
    } else {
      textElement.setTextContent("No " + stringTitle);
    }

    for (Measurement measurement : measurements) {
      POCDMT000040Entry entry = buildOneObservation(measurement);
      section.getEntry().add(entry);
    }

    retVal.setSection(section);
    return retVal;
  }


  private void buildVitalSignsSection(List<Measurement> measurements) {
    List<String> templateIds = new ArrayList<String>(Arrays.asList(HL7.PHMR_VITAL_SIGNS_FIRST_ID, HL7.PHMR_ENTRY_ID));
    CE ce = createCode("8716-3","2.16.840.1.113883.6.1");
    String stringTitle = "Vital Signs";

    POCDMT000040Component3 vitalSigns = createGenericMeasurements(templateIds, ce, stringTitle, measurements);

    result.getComponent().getStructuredBody().getComponent().add(vitalSigns);
  }

  private void buildResultsSection(List<Measurement> measurements) {
    log.debug("Creating observations from " + measurements.size() + " measurements");
    List<String> templateIds = new ArrayList<String>(Arrays.asList(HL7.PHMR_RESULTS_FIRST_ID,HL7.PHMR_RESULTS_SECOND_ID));
    CE ce = createCode("30954-2","2.16.840.1.113883.6.1");
    String stringTitle = "Results";
    
    POCDMT000040Component3 results = createGenericMeasurements(templateIds, ce, stringTitle, measurements);

    result.getComponent().getStructuredBody().getComponent().add(results);
  }

  private void buildMedicalEquipmentSection(List<MedicalEquipment> equipment) {
    POCDMT000040Component3 instruments = createInstrumentSection(equipment);

    result.getComponent().getStructuredBody().getComponent().add(instruments);

  }


  /**
   * Create instrument section from list of instruments
   * @param instruments The list to create instrument from
   * @return Section of instruments
   */
  private POCDMT000040Component3 createInstrumentSection(List<MedicalEquipment> instruments) {
    POCDMT000040Component3 retVal = new POCDMT000040Component3();
    retVal.setTypeCode(ActRelationshipHasComponent.COMP);
    retVal.setContextConductionInd(Boolean.TRUE);

    POCDMT000040Section section = new POCDMT000040Section();
    section.getClassCode().add("DOCSECT");
    section.getMoodCode().add("EVN");

    List<String> templateIds = new ArrayList<String>(Arrays.asList(HL7.PHMR_MEDICAL_EQUIPMENT_FIRST_ID,
        HL7.PHMR_MEDICAL_EQUIPMENT_SECOND_ID));
    for (String id : templateIds) {
      II ii = new II();
      ii.setRoot(id);
      section.getTemplateId().add(ii);
    }

    section.setCode(createCode("46264-8","2.16.840.1.113883.6.1"));

    String titelString = "Medical Equipment";
    section.setTitle(generateTitle(titelString));

    Element textElement = docContextForCreatingTextSections.createElement("text");
    textElement.setAttribute("xmlns", "urn:hl7-org:v3");
    section.setText(textElement);

    if (instruments.size() > 0) {
      StringBuffer sb = new StringBuffer();
      // Jaxb and HMTL tags does not play nice, so using printf instead

      for (MedicalEquipment instrument : instruments) {
        if (instrument.getMedicalDeviceCode() != null) {
          sb.append(instrument.getMedicalDeviceCode()).append(", ");
        }
        if (instrument.getManufacturerModelName() != null) {
          sb.append(instrument.getManufacturerModelName()).append(", ");
        }
        if (instrument.getMedicalDeviceDisplayName() != null) {
          sb.append(instrument.getMedicalDeviceDisplayName()).append(", ");
        }
        if (instrument.getSoftwareName() != null) {
          sb.append(instrument.getSoftwareName());
        }
        sb.append("\n");
      }
      sb.append("\n");

      // TODO: encode this for XML?
      textElement.setTextContent(sb.toString());
    } else {
      textElement.setTextContent("No " + titelString );
    }

    for (MedicalEquipment i : instruments) {
      POCDMT000040Entry entry = createEntryFromMedicalEquipment(i);
      if (entry != null) {
        section.getEntry().add(entry);
      }
    }

    retVal.setSection(section);
    return retVal;
  }



  /**
   * Creates PHMR device entry section from Instrument object
   * @param i The Instrument to use
   * @return
   */
  private POCDMT000040Entry createEntryFromMedicalEquipment(MedicalEquipment i) {
    POCDMT000040Entry entry = null;

    if (i != null) {

      String instrumentDevice = i.getManufacturerModelName();
      String instrumentVersion = i.getSoftwareName();

      POCDMT000040Participant2 participant = createParticipant(instrumentDevice, instrumentVersion);
      CE code = createCode(i.getMedicalDeviceCode(),"1.2.208.184.100.2", null, i.getMedicalDeviceDisplayName());
      participant.getParticipantRole().getPlayingDevice().setCode(code);
      POCDMT000040Organizer organizer = createOrganizer(participant);

      entry = createEntry(organizer);
    }

    return entry;
  }

  

  // === Private helper methods

  private POCDMT000040Entry buildOneObservation(Measurement measurement) {

    POCDMT000040Entry entry;
    
    // CONF-PHMR-58
    entry = new POCDMT000040Entry();
    entry.setContextConductionInd(Boolean.TRUE);
    entry.setTypeCode(XActRelationshipEntry.COMP);


    // PHMR CONF-381
    POCDMT000040Organizer organizer = new POCDMT000040Organizer();

    organizer.setClassCode(XActClassDocumentEntryOrganizer.CLUSTER);
    organizer.getMoodCode().add(XDocumentActMood.EVN.value());
    
    entry.setOrganizer(organizer);

    II ii2 = new II();
    ii2.setRoot(HL7.OBSERVATION_ORGANIZER);
    organizer.getTemplateId().add(ii2);

    CS cs = new CS();

    String statusAsString = null;
    if (measurement.getStatus() == Status.COMPLETED) {
      statusAsString = "completed";
    } else if (measurement.getStatus() == Status.NULLIFIED) {
      statusAsString = "nullified";
    }
    cs.setCode(statusAsString);
    organizer.setStatusCode(cs);

    IVLTSExplicit effectiveTime = new IVLTSExplicit();
    effectiveTime.setValue(dateTimeformatter.format((measurement.getTimestamp() != null ? measurement.getTimestamp() : new Date()) ));
    organizer.setEffectiveTime(effectiveTime);

    POCDMT000040Component4 component4 = new POCDMT000040Component4();
    component4.setContextConductionInd(Boolean.TRUE);
    component4.setTypeCode(ActRelationshipHasComponent.COMP);

    POCDMT000040Observation observation = new POCDMT000040Observation();
//        observation.setEffectiveTime(effectiveTime);
    observation.getClassCode().add("OBS");
    observation.setMoodCode(XActMoodDocumentObservation.EVN);

    List<String> templateIds = new ArrayList<String>(Arrays.asList(HL7.OBSERVATION));

    for (String id : templateIds) {
      II ii = new II();
      ii.setRoot(id);
      observation.getTemplateId().add(ii);
    }

    CDExplicit cd = new CDExplicit();
    if (measurement.getType() == Measurement.Type.PHYSICAL_QUANTITY) {

      // CONF-PHMR-58
      II numericId = new II();
      numericId.setRoot(HL7.PHMR_NUMERIC_OBSERVATION);
      observation.getTemplateId().add(numericId);

      cd.setCode(measurement.getCode());
      cd.setCodeSystem(measurement.getCodeSystem());
      cd.setDisplayName(measurement.getDisplayName());
      cd.setCodeSystemName(measurement.getCodeSystemName());

      PQ value = factory.createPQ();
      value.setValue(measurement.getValue());
      value.setUnit(measurement.getUnit());

      observation.getValue().add(value);

    } else {
      cd.getNullFlavor().add("NI");
    }
    observation.setCode(cd);
    component4.setObservation(observation);
    organizer.getComponent().add(component4);

    // PHMR CONF-381

    if (measurement.getType() == Measurement.Type.OBSERVATION_MEDIA) {

      // TODO: validate this section.
      POCDMT000040EntryRelationship entryRelationship = factory.createPOCDMT000040EntryRelationship();
      entryRelationship.setContextConductionInd(Boolean.TRUE);
      entryRelationship.setTypeCode(XActRelationshipEntryRelationship.COMP);

      POCDMT000040ObservationMedia observationMedia = factory.createPOCDMT000040ObservationMedia();
      observationMedia.getMoodCode().add("EVN");
      observationMedia.getClassCode().add("OBS");
      observationMedia.setIdAttr("UrlToMedia");

      entryRelationship.setObservationMedia(observationMedia);
      observation.getEntryRelationship().add(entryRelationship);


      // TODO: validate OID and parameterize extension.
      II id = createId(MedCom.ROOT_OID, MedCom.ROOT_AUTHORITYNAME,measurement.getId());
      observationMedia.getId().add(id);


      EDExplicit value = factory.createEDExplicit();
      TELExplicit t = factory.createTELExplicit();
      t.setValue(measurement.getReference());
      JAXBElement<TELExplicit> edExplicitReference = factory.createEDExplicitReference(t);
      value.getContent().add(edExplicitReference);

      observationMedia.setValue(value);
    }

    // In case of context information, make a methodCode

    if (measurement.getContext() != null) {
      String performerCode = PerformerMethodCode(measurement);
      String performerDisplayName = PerformerMethodDisplayName(measurement);
      String participantRoleCode = PerformerMethodParticipantRole(measurement);

      CE ce = new CE();

      ce.setCode(performerCode);
      ce.setCodeSystem(MedCom.MESSAGECODE_OID);
      ce.setDisplayName(performerDisplayName);
      ce.setCodeSystemName(MedCom.MESSAGECODE_DISPLAYNAME);

      observation.getMethodCode().add(ce);

      String provisionCode = ProvisionMethodCode(measurement);
      String provisionDisplayName = ProvisionMethodDisplayName(measurement);

      ce = new CE();

      ce.setCode(provisionCode);
      ce.setCodeSystem(MedCom.MESSAGECODE_OID);
      ce.setDisplayName(provisionDisplayName);
      ce.setCodeSystemName(MedCom.MESSAGECODE_DISPLAYNAME);

      observation.getMethodCode().add(ce);

      // TODO: Validate
      if (measurement.getContext() != null && measurement.getContext().getOrganizationIdentity() != null) {
        POCDMT000040Participant2 participant2 = factory.createPOCDMT000040Participant2();

        participant2.getTypeCode().add("EN");
        participant2.setContextControlCode("OP");
        POCDMT000040ParticipantRole patientRole = factory.createPOCDMT000040ParticipantRole();
        patientRole.getClassCode().add(participantRoleCode);

        patientRole.getId().add(createID(NSI.SOR_OID,measurement.getContext().getOrganizationIdentity().getOrgOID()));

        participant2.setParticipantRole(patientRole);

        observation.getParticipant().add(participant2);
      }
    }
    // entryRelationship
    if (measurement.hasComment()) {
      org.net4care.phmr.model.Comment comment = measurement.getComment();

      POCDMT000040EntryRelationship entryRelationship = factory.createPOCDMT000040EntryRelationship();
      entryRelationship.setContextConductionInd(Boolean.TRUE);
      entryRelationship.setTypeCode(XActRelationshipEntryRelationship.COMP);
      observation.getEntryRelationship().add(entryRelationship);

      POCDMT000040Act act = factory.createPOCDMT000040Act();
      entryRelationship.setAct(act);

      act.setClassCode(XActClassDocumentEntryAct.ACT);
      act.setMoodCode(XDocumentActMood.EVN);

      CD loinc = new CD();
      loinc.setCode(Loinc.COMMENT);
      loinc.setCodeSystem(Loinc.OID);
      loinc.setCodeSystemName(Loinc.DISPLAYNAME);
      loinc.setDisplayName(Loinc.COMMENT_DISPLAYNAME);

      act.setCode(loinc);

      EDExplicit edExplicit = factory.createEDExplicit();
      edExplicit.getContent().add(comment.getText());
      act.setText(edExplicit);
      
      POCDMT000040Author author = factory.createPOCDMT000040Author();
      author.setContextControlCode("OP");
      author.getTypeCode().add("AUT");
      act.getAuthor().add(author);

      TSExplicit authorTime = factory.createTSExplicit();
      authorTime.setValue(dateTimeformatter.format(comment.getTime()));
      author.setTime(authorTime);

      POCDMT000040AssignedAuthor assignedAuthor = factory.createPOCDMT000040AssignedAuthor();
      assignedAuthor.setClassCode("ASSIGNED");
      author.setAssignedAuthor(assignedAuthor);

      String organizationId = comment.getOrganization().getOrgOID();
      assignedAuthor.getId().add(createId(NSI.SOR_OID, NSI.SOR_AUTHORITYNAME, organizationId));

      POCDMT000040Person assignedPerson = factory.createPOCDMT000040Person();
      assignedPerson.getClassCode().add("PSN");
      assignedPerson.setDeterminerCode("INSTANCE");

      assignedAuthor.setAssignedPerson(assignedPerson);
      assignedPerson.getName().add(generateName(comment.getAuthor()));
    }

    return entry;

  }

  // === Helper methods ===

  // TODO: REMOVE THIS
  @Override
  public void buildCopyrightSection(String text) {

  }

  @Override
  public void buildSections(List<Section> sectionList) {
    List<Measurement> theMeasurementList;

    // Vital Signs
    Section result = sectionList.get(0);
    theMeasurementList = extractMeasurementsFromSection(result);
    buildVitalSignsSection(theMeasurementList);

    // Results
    result = sectionList.get(1);
    theMeasurementList = extractMeasurementsFromSection(result);
    buildResultsSection(theMeasurementList);
    
    // Equipment
    result = sectionList.get(2);
    List<MedicalEquipment> theEquipmentList = new ArrayList<MedicalEquipment>(5);
    for(Entry e : result.getEntryList()) {
      Observation obs = e.getObservation();
      MedicalEquipment m = (MedicalEquipment) obs.getContents().get("equipment");
      theEquipmentList.add(m);
    }
    buildMedicalEquipmentSection(theEquipmentList);
  }
  
  private List<Measurement> extractMeasurementsFromSection(Section result) {
    List<Measurement> theMeasurementList;
    theMeasurementList = new ArrayList<Measurement>(10);
    
    for (Entry e : result.getEntryList() ) {
      Observation obs = e.getObservation();
      Measurement m = (Measurement) obs.getContents().get("measurement");
      theMeasurementList.add(m);
    }
    return theMeasurementList;
  }

  private String ProvisionMethodCode(Measurement measurement) {
    switch (measurement.getContext().getMeasurementProvisionMethodCode()) {
      case Electronically:
        return MedCom.TRANSFERRED_ELECTRONICALLY;
      case TypedByCitizen:
        return MedCom.TYPED_BY_CITIZEN;
      case TypedByCitizenRelative:
        return MedCom.TYPED_BY_CITIZEN_RELATIVE;
      case TypedByHealthcareProfessional:
        return MedCom.TYPED_BY_HEALTHCAREPROFESSIONAL;
      case TypedByCareGiver:
        return MedCom.TYPED_BY_CAREGIVER;
      default:
        throw new IllegalStateException("Invalid Context provision method code" + measurement.getContext().getMeasurementProvisionMethodCode().toString());
    }
  }

  private String ProvisionMethodDisplayName(Measurement measurement) {
    switch (measurement.getContext().getMeasurementProvisionMethodCode()) {
      case Electronically:
        return MedCom.TRANSFERRED_ELECTRONICALLY_DISPLAYNAME;
      case TypedByCitizen:
        return MedCom.TYPED_BY_CITIZEN_DISPLAYNAME;
      case TypedByCitizenRelative:
        return MedCom.TYPED_BY_CITIZEN_RELATIVE_DISPLAYNAME;
      case TypedByHealthcareProfessional:
        return MedCom.TYPED_BY_HEALTHCAREPROFESSIONAL_DISPLAYNAME;
      case TypedByCareGiver:
        return MedCom.TYPED_BY_CAREGIVER_DISPLAYNAME;
      default:
        throw new IllegalStateException("Invalid Context provision method code" + measurement.getContext().getMeasurementProvisionMethodCode().toString());
    }
  }

  private String PerformerMethodCode(Measurement measurement) {
    switch (measurement.getContext().getMeasurementPerformerCode()) {
      case Citizen:
        return MedCom.PERFORMED_BY_CITIZEN;
      case HealthcareProfessional:
        return MedCom.PERFORMED_BY_HEALTHCAREPROFESSIONAL;
      case CareGiver:
        return MedCom.PERFORMED_BY_CAREGIVER;
      default:
        throw new IllegalStateException("Invalid Context performer code" + measurement.getContext().getMeasurementPerformerCode().toString());
    }
  }

  private String PerformerMethodDisplayName(Measurement measurement) {
    switch (measurement.getContext().getMeasurementPerformerCode()) {
      case Citizen:
        return MedCom.PERFORMED_BY_CITIZEN_DISPLAYNAME;
      case HealthcareProfessional:
        return MedCom.PERFORMED_BY_HEALTHCAREPROFESSIONAL_DISPLAYNAME;
      case CareGiver:
        return MedCom.PERFORMED_BY_CAREGIVER_DISPLAYNAME;
      default:
        throw new IllegalStateException("Invalid Context performer code" + measurement.getContext().getMeasurementPerformerCode().toString());
    }
  }

  private String PerformerMethodParticipantRole(Measurement measurement) {
    switch (measurement.getContext().getMeasurementPerformerCode()) {
      case Citizen:
        return "CIT";
      case HealthcareProfessional:
        return "ASSIGNED";
      case CareGiver:
        return "CAREGIVER";
      default:
        throw new IllegalStateException("Invalid Context performer code" + measurement.getContext().getMeasurementPerformerCode().toString());
    }
  }
}
