package org.net4care.phmr.builders;

import java.text.*;
import java.io.*;
import java.math.BigInteger;
import java.util.*;

import javax.xml.bind.*;
import javax.xml.parsers.*;

import org.hl7.v3.*;
import org.net4care.core.*;
import org.net4care.phmr.codes.*;
import org.net4care.phmr.model.*;
import org.slf4j.*;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public abstract class BaseClinicalDocumentBuilder implements ClinicalDocumentBuilder {

  protected static Logger log = LoggerFactory.getLogger(DanishPHMRBuilder.class);
  protected static JAXBContext jaxbContext;
  protected POCDMT000040ClinicalDocument result;
  private String uniqueId;
  protected UUIDStrategy uuidStrategy;
  protected ObjectFactory factory;
  protected DocumentBuilder docBuilder;
  protected Document docContextForCreatingTextSections;
  
  protected SectionExtender sectionExtender;
  
  /** dateTimeformatter that can translate into the HL7 formatting. */
  protected static Format dateTimeformatter = new SimpleDateFormat("yyyyMMddHHmmssZZZZ");
  protected static Format dateFormatter = new SimpleDateFormat("yyyyMMdd");

  static {
    try {
      jaxbContext = JAXBContext.newInstance(POCDMT000040ClinicalDocument.class);
    } catch (JAXBException e) {
      log.error("Could not create JAXBContext during class loading", e);
      System.out.println("ERROR: Please review the log file!");
    }
  }

  /** Construct a builder that can produce Danish CDA documents.
   * Will generate random UUID in the document.
   */
  public BaseClinicalDocumentBuilder() {
    this(new UUIDStrategy() {
      @Override
      public String generate() {
        return java.util.UUID.randomUUID().toString();
      }
    });
  }
  
  /** Construct a builder that can produce a java XML document
   * containing a correctly formatted Danish CDA. This
   * variant of the constructor should only be used for testing!
   *
   * @param uuidStrategy the strategy to generate UUID for
   * the document. Allows tests to define a fixed UUID.
   * instance which will generate a new random UUID.
   */
  public BaseClinicalDocumentBuilder(UUIDStrategy uuidStrategy) {
    this(uuidStrategy, new NullSectionExtender());
  }


  public BaseClinicalDocumentBuilder(UUIDStrategy uuidStrategy,
      SectionExtender sectionExtender) {
    this.uuidStrategy = uuidStrategy;

    // Create context for creating text sections
    DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
    docBuilder = null;
    try {
      docBuilder = dbfac.newDocumentBuilder();
    } catch (ParserConfigurationException e) {
      log.error("Error in Parser configuration", e);
      System.out.println("ERROR: Please review the log file!");
    }
    docContextForCreatingTextSections = docBuilder.newDocument();
    
    factory = new ObjectFactory();

    this.sectionExtender = sectionExtender;
    
    result = null;
  }

  /** Get the XML document in the POCDMT000040ClinicalDocument 
   * format. 
   * @return the POCDMT000040ClinicalDocument
   */
  public POCDMT000040ClinicalDocument getClinicalDocument() {
    return result;
  }

  /** Get the XML document in standard DOM format.
   * @param stylesheet the style sheet to use
   * @return the DOM format
   */
  public Document getDocument(String stylesheet) {
    Document doc = null;
    try {
      String cdaString = getDocumentAsString(stylesheet);
      doc = docBuilder.parse(new ByteArrayInputStream(cdaString.getBytes("UTF-8")));
    } catch (SAXException e) {
      log.error("Could not parse document (1) ", e);
    } catch (IOException e) {
      log.error("Could not parse document (2) ", e);
    } catch (JAXBException e) {
      log.error("Could not parse document (3) ", e);
    }
    return doc;
  }

  /** Get the XML document in standard DOM format.
   * @return the DOM format
   */
  public Document getDocument() {
    return getDocument(null);
  }

  /** Get the XML document as a String.
   * @param stylesheet the style sheet to use
   * @return the XML string
   * @throws JAXBException if the stylesheet string is
   * illformed.
   */
  public String getDocumentAsString(String stylesheet) throws JAXBException {
  
    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
    StringWriter stringWriter = new StringWriter();
    ObjectFactory of = new ObjectFactory();
  
    // output pretty printed
    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    //  jaxbMarshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
  
    if (stylesheet != null) {
      jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders",
          "<?xml-stylesheet type='text/xsl' href='" + stylesheet + "' ?>");
    }
  
    jaxbMarshaller.marshal(of.createClinicalDocument(result), stringWriter);
    return stringWriter.toString();
  }

  /** Get the XML document as a String.
   * @return the XML string
   */
  public String getDocumentAsString() {
    String docAsString = null;
    try {
      docAsString = getDocumentAsString(null);
    } catch (JAXBException e) {
      log.error("Could not convert document to string", e);
    }
    return docAsString;
  }

  @Override
  public void buildRootNode() {
    // CONF-PHMR-1:
    result = factory.createPOCDMT000040ClinicalDocument();
    result.setClassCode(ActClassClinicalDocument.DOCCLIN);
    result.getMoodCode().add(XDocumentActMood.EVN.value());
  }

  /** Create ID
   * @param root
   * @param extension
   * @return
   */
  protected II createID(String root, String extension) {
    II id = factory.createII();
    id.setRoot(root);
    id.setExtension(extension);
    return id;
  }

  private II createID(String root) {
    II id = factory.createII();
    id.setRoot(root);
    return id;
  }

  /**
   * Create code based on code and codeSystem
   * @param code
   * @param codeSystem
   * @return
   */
  protected CE createCode(String code, String codeSystem) {
    return createCode(code,codeSystem, null, null);
  }

  /**
   * Create code based on code, code system and codesystem name
   * @param code
   * @param codeSystem
   * @param codeSystemName
   * @return
   */
  private CE createCode(String code, String codeSystem, String codeSystemName) {
    return createCode(code,codeSystem,codeSystemName,null);
  }

  /**
   * Create code type
   * @param code the code to be sued
   * @param codeSystem codesystem
   * @param codeSystemName name
   * @param displayName display name
   * @return
   */
  protected CE createCode(String code, String codeSystem, String codeSystemName,
      String displayName) {
        CE ce = new CE();
        if (code != null && code.length() > 0) {
          ce.setCode(code);
        }
        if (codeSystem != null && codeSystem.length() > 0 ) ce.setCodeSystem(codeSystem);
        if (codeSystemName != null && codeSystemName.length() > 0) ce.setCodeSystemName(codeSystemName);
        if (displayName != null && displayName.length() > 0) ce.setDisplayName(displayName);
      
        return ce;
      }

  @Override
  public void buildHeader(CDAHeaderData data) {
    // The sequencing is important of the following tags (and not
    // quite what is defined in the PHMR report!), refer to
    // Boone chapter 14.
  
    // CONF-PHMR-? - SECTION 2.6
    POCDMT000040InfrastructureRootTypeId rootTypeId = new POCDMT000040InfrastructureRootTypeId();
    rootTypeId.setExtension(HL7.PHMR_DK_TYPEID_EXTENSION);
    rootTypeId.setRoot(HL7.PHMR_TYPEID_ROOT);
    result.setTypeId(rootTypeId);
  
    // CONF-DK PHMR-2
    II templateId = createID(data.getTemplateId());
    result.getTemplateId().add(templateId);
  
    // CONF-PHMR-DK-19
    uniqueId = uuidStrategy.generate();
  
    II id = createId(MedCom.ROOT_OID, MedCom.ROOT_AUTHORITYNAME, uniqueId);
    result.setId(id);
  
    // CONF-PHMR-3:
    String codeAsString = data.getCodeCode();
    String displayName = data.getCodeDisplayName();
    CE code = createCode(codeAsString, Loinc.OID, Loinc.DISPLAYNAME, displayName);
    result.setCode(code);
  }

  @Override
  public void buildContext(String theTitle, PersonIdentity patientIdentity, Date effectiveTime,
      String setId, String versionNumber) {
        // CONF-PHMR-15
        STExplicit title = factory.createSTExplicit();
        title.getContent().add(theTitle);
        result.setTitle(title);
      
        // CONF-PHMR-16 / CONF-DK PHMR-17
        TSExplicit effectiveTimeTsExplicit = factory.createTSExplicit();
        effectiveTimeTsExplicit.setValue(dateTimeformatter.format(effectiveTime));
        result.setEffectiveTime(effectiveTimeTsExplicit);
      
      
        // Section 2.9
        CE confidentialityCode = createCode("N",HL7.CONFIDENTIALITY_OID);
        result.setConfidentialityCode(confidentialityCode);
      
        // CONF-PHMR-17-20
        CS languageCode = factory.createCS();
        languageCode.setCode("da-DK");
        result.setLanguageCode(languageCode);
      
        // CONF-DK PHMR-22
        II setIdII = createID(MedCom.MESSAGECODE_OID,setId);
        result.setSetId(setIdII);
      
        INT version = new INT();
        version.setValue(BigInteger.valueOf(Long.valueOf(versionNumber)));
        result.setVersionNumber(version);
      }

  protected II createId(String root, String authorityName, String extension) {
    II id = factory.createII();
    id.setAssigningAuthorityName(authorityName);
    id.setExtension(extension);
    id.setRoot(root);
    return id;
  }

  /**
   * Generate address based on citizen
   * @param addressData used for constructing JAXB objets
   * @return the populated ADExplicit object
   */
  private ADExplicit generateAddress(AddressData addressData) {
    org.hl7.v3.ObjectFactory factory = new org.hl7.v3.ObjectFactory();
    ADExplicit address = (ADExplicit) (factory.createADExplicit());
    List<Serializable> addrlist = address.getContent();
  
  
    if ( addressData != null && addressData.getStreet().length > 0
        && addressData.getPostalCode() != null
        && addressData.getCity() != null) {
  
      if (AddressData.Use.WorkPlace.equals(addressData.getAddressUse())) {
        address.getUse().add("WP");
      }
  
      if (AddressData.Use.HomeAddress.equals(addressData.getAddressUse())) {
        address.getUse().add("H");
      }
  
      // Street
      for (String street : addressData.getStreet()) {
        AdxpExplicitStreetAddressLine streetHl7 = new AdxpExplicitStreetAddressLine();
        streetHl7.setContent(street);
        addrlist.add(factory.createADExplicitStreetAddressLine(streetHl7));
      }
  
      // Zip code
      AdxpExplicitPostalCode zipHl7 = new AdxpExplicitPostalCode();
      zipHl7.setContent(addressData.getPostalCode());
      addrlist.add(factory.createADExplicitPostalCode(zipHl7));
  
      // City
      AdxpExplicitCity cityHl7 = new AdxpExplicitCity();
      cityHl7.setContent(addressData.getCity());
      addrlist.add(factory.createADExplicitCity(cityHl7));
  
  
      // Country
      if (addressData.getCountry() != null) {
        AdxpExplicitCountry countryHl7 = new AdxpExplicitCountry();
        countryHl7.setContent(addressData.getCountry());
        addrlist.add(factory.createADExplicitCountry(countryHl7));
      }
    } else {
      address.getNullFlavor().add("NI");
    }
  
    return address;
  }

  @Override
  public void buildPatientSection(PersonIdentity patientIdentity) {
    POCDMT000040RecordTarget recordTarget = new POCDMT000040RecordTarget();
  
    // CONF-PHMR-24-28
    recordTarget.setContextControlCode("OP");
    recordTarget.getTypeCode().add("RCT");
  
    //
    POCDMT000040PatientRole patientRole = new POCDMT000040PatientRole();
    patientRole.getClassCode().add("PAT");
  
  
    // If the person is not defined, add a null flavor
    if (patientIdentity.getNullFlavor() != null) {
      II id = factory.createII();
      id.getNullFlavor().add("NI");
      patientRole.getId().add(id);
      
    } else {
      // Decision - we use CPR number as ID under the codeSystem
      // of Danish CPR
      II danishCprII = createId(NSI.CPR_OID, NSI.CPR_AUTHORITYNAME, patientIdentity.getSSN());
  
      patientRole.getId().add(danishCprII);
  
      // ADDRESS
      // CONF-PHMR-5
      for (Telecom tele : patientIdentity.getTelecomList()) {
        patientRole.getTelecom().add(
            generateTelecom(tele.getValue(), tele.getProtocol(),
                translateToHL7String(tele.getAddressUse())));
      }
  
      patientRole.getAddr().add(generateAddress(patientIdentity.getAddress()));
  
      // PATIENT
      // The CDA book p 161
      // CONF-PHMR-4
      POCDMT000040Patient patient = new POCDMT000040Patient();
      patient.getClassCode().add("PSN");
      patient.setDeterminerCode("INSTANCE");
      patient.getName().add(generateName(patientIdentity));
  
      String gender;
      switch (patientIdentity.getGender()) {
      case Female:
        gender = "F";
        break;
      case Male:
        gender = "M";
        break;
      default: // Undifferentiated
        gender = "UN";
        break;
      }
  
      patient.setAdministrativeGenderCode(createCode(gender, HL7.GENDER_OID));
  
      TSExplicit birth = new TSExplicit();
      birth.setValue(dateFormatter.format(patientIdentity.getBirthTime()));
      patient.setBirthTime(birth);
  
      patientRole.setPatient(patient);
    }
    
    recordTarget.setPatientRole(patientRole);
    result.getRecordTarget().add(recordTarget);
    
  }

  @Override
  public void buildAuthorSection(OrganizationIdentity authorOrganizationIdentity, PersonIdentity authorIdentity, Date authorParticipationStartTime) {
  
    // Section 2.13.2
    // The CDA book p 151
    // TODO: Refactor to remove code duplicated from patient section.
  
    POCDMT000040Author author = new POCDMT000040Author();
    TSExplicit time = new TSExplicit();
    POCDMT000040Person assignedPerson = factory.createPOCDMT000040Person();
    POCDMT000040AssignedAuthor assignedAuthor = new POCDMT000040AssignedAuthor();
    POCDMT000040Organization representedOrganization = factory.createPOCDMT000040Organization();
  
    ADExplicit authorOrganizationAddress = new ADExplicit();
    ADExplicit addr = new ADExplicit(); // TODO
    TELExplicit telecom = new TELExplicit();
    TELExplicit email = new TELExplicit();
    PNExplicit name = new PNExplicit(); // TODO
  
    author.setContextControlCode(ContextControlPropagating.OP.value());
    author.getTypeCode().add(XParticipationAuthorPerformer.AUT.value());
  
    if (authorParticipationStartTime != null) {
      time.setValue(dateTimeformatter.format(authorParticipationStartTime));
    }
  
    if (authorOrganizationIdentity != null) {
      assignedAuthor.setClassCode(XInformationRecipientRole.ASSIGNED.value());
      II organisationId = createId(NSI.SOR_OID, NSI.SOR_AUTHORITYNAME, authorOrganizationIdentity.getOrgOID());
      assignedAuthor.getId().add(organisationId);
  
      // ADDRESS
      // CONF-PHMR-5
  
      for (Telecom tele : authorOrganizationIdentity.getTelecomList()) {
        assignedAuthor.getTelecom().add(generateTelecom(tele.getValue(), tele.getProtocol(),
            translateToHL7String(tele.getAddressUse())));
      }
  
      assignedPerson.setDeterminerCode("INSTANCE");
      assignedPerson.getClassCode().add("PSN");
  
      name = generateName(authorIdentity);
  
  
      representedOrganization.setClassCode("ORG");
      representedOrganization.setDeterminerCode("INSTANCE");
      ONExplicit onExplicit = factory.createONExplicit();
      onExplicit.getContent().add(authorOrganizationIdentity.getOrgName());
      representedOrganization.getName().add(onExplicit);
  
      authorOrganizationAddress = generateAddress(authorOrganizationIdentity.getAddress());
  
    } else {
      // Set up nullFlavour
      time.getNullFlavor().add("NI");
      authorOrganizationAddress.getNullFlavor().add("NI");
      name.getNullFlavor().add("NI");
      telecom.getNullFlavor().add("NI");
      assignedAuthor.getTelecom().add(telecom);
    }
    // Set them into reply
    assignedPerson.getName().add(name);
  
    assignedAuthor.getAddr().add(authorOrganizationAddress);
    assignedAuthor.setAssignedPerson(assignedPerson);
    assignedAuthor.setRepresentedOrganization(representedOrganization);
  
  
    author.setAssignedAuthor(assignedAuthor);
    author.setTime(time);
  
    result.getAuthor().add(author);
  }

  @Override
  public void buildCustodianSection(OrganizationIdentity custodianIdentity) {
    POCDMT000040Custodian custodian = new POCDMT000040Custodian();
    custodian.getTypeCode().add(CodeSystem.CST.value());
    result.setCustodian(custodian);
  
    POCDMT000040AssignedCustodian assignedCustodian = new POCDMT000040AssignedCustodian();
    assignedCustodian.setClassCode(XInformationRecipientRole.ASSIGNED.value());
  
    POCDMT000040CustodianOrganization representedCustodianOrganization = new POCDMT000040CustodianOrganization();
    representedCustodianOrganization.setClassCode(ParticipationTargetLocation.ORG.value());
    representedCustodianOrganization.setDeterminerCode(XDeterminerInstanceKind.INSTANCE.value());
  
    assignedCustodian.setRepresentedCustodianOrganization(representedCustodianOrganization);
    custodian.setAssignedCustodian(assignedCustodian);
  
    if (custodianIdentity != null) {
  
      II id = createId(NSI.SOR_OID, NSI.SOR_AUTHORITYNAME,custodianIdentity.getOrgOID());
      representedCustodianOrganization.getId().add(id);
  
      ONExplicit name = factory.createONExplicit();
      name.getContent().add(custodianIdentity.getOrgName());
      representedCustodianOrganization.setName(name);
  
      // Only one telecom element.
      // TODO: verify that for custodian, only 1 telecom element can be used.
      if (custodianIdentity.getTelecomList().length >= 1) {
        Telecom tele = custodianIdentity.getTelecomList()[0];
        representedCustodianOrganization.setTelecom(generateTelecom(tele.getValue(),tele.getProtocol(),translateToHL7String(tele.getAddressUse())));
      }
  
      ADExplicit addr = generateAddress(custodianIdentity.getAddress());
      representedCustodianOrganization.setAddr(addr);
    } else {
      II ii = new II();
      ii.getNullFlavor().add("NI");
      representedCustodianOrganization.getId().add(ii);
  
      ONExplicit name = new   ONExplicit(); // TODO
      name.getNullFlavor().add("NI");
      representedCustodianOrganization.setName(name);
  
      TELExplicit telecom = new TELExplicit();
      telecom.getNullFlavor().add("NI");
      representedCustodianOrganization.setTelecom(telecom);
  
      ADExplicit addr = new ADExplicit(); // TODO
      addr.getNullFlavor().add("NI");
      representedCustodianOrganization.setAddr(addr);
    }
  }

  @Override
  public void buildLegalAuthenticatorSection(OrganizationIdentity authenticatorOrganizationIdentity, PersonIdentity authenticatorPersonIdentity,
      Date timeOfAutentication) {
        // QFD documents does not use this section!
        if (authenticatorOrganizationIdentity == null ||
            authenticatorPersonIdentity == null) {
          return;
        }
        
        // Section 2.13.4
        POCDMT000040LegalAuthenticator legalAuthenticator = factory.createPOCDMT000040LegalAuthenticator();
        legalAuthenticator.setContextControlCode("OP");
        legalAuthenticator.getTypeCode().add("LA");
      
        TSExplicit time = factory.createTSExplicit();
        if (timeOfAutentication != null) {
          time.setValue(dateTimeformatter.format(timeOfAutentication));
        } else {
          time.getNullFlavor().add("NI");
        }
      
        legalAuthenticator.setTime(time);
      
        CS signatureCode = factory.createCS();
        signatureCode.getNullFlavor().add("NI");
      
        legalAuthenticator.setSignatureCode(signatureCode);
      
        // Assigned entity
        POCDMT000040AssignedEntity assignedEntity = factory.createPOCDMT000040AssignedEntity();
        assignedEntity.setClassCode("ASSIGNED");
        legalAuthenticator.setAssignedEntity(assignedEntity);
      
        II id = createId(NSI.SOR_OID, NSI.SOR_AUTHORITYNAME, authenticatorOrganizationIdentity.getOrgOID());
        assignedEntity.getId().add(id);
      
        ADExplicit addr = factory.createADExplicit();
        addr = generateAddress(authenticatorOrganizationIdentity.getAddress());
        assignedEntity.getAddr().add(addr);
      
        for (Telecom tele : authenticatorOrganizationIdentity.getTelecomList()) {
          TELExplicit telExplicit = generateTelecom(tele.getValue(),
              tele.getProtocol(),translateToHL7String(tele.getAddressUse()));
          assignedEntity.getTelecom().add(telExplicit);
        }
      
        POCDMT000040Person assignedPerson = factory.createPOCDMT000040Person();
        assignedPerson.getClassCode().add("PSN");
        assignedPerson.setDeterminerCode("INSTANCE");
      
        PNExplicit name = generateName(authenticatorPersonIdentity);
        assignedPerson.getName().add(name);
      
        legalAuthenticator.getAssignedEntity().setAssignedPerson(assignedPerson);
      
        POCDMT000040Organization representedOrganization = factory.createPOCDMT000040Organization();
        representedOrganization.setClassCode("ORG");
        representedOrganization.setDeterminerCode("INSTANCE");
      
        ONExplicit orgName = factory.createONExplicit();
        orgName.getContent().add(authenticatorOrganizationIdentity.getOrgName());
      
        representedOrganization.getName().add(orgName);
        assignedEntity.setRepresentedOrganization(representedOrganization);
      
        result.setLegalAuthenticator(legalAuthenticator);
      }

  @Override
  public void buildDocumentationOf(Date serviceStart, Date serviceEnd) {
    POCDMT000040DocumentationOf documentationOf = new POCDMT000040DocumentationOf();
    documentationOf.getTypeCode().add("DOC");
  
    POCDMT000040ServiceEvent serviceEvent = new POCDMT000040ServiceEvent();
    serviceEvent.getClassCode().add("MPROT");
    serviceEvent.getMoodCode().add("EVN");
  
    IVLTSExplicit effectiveTime = new IVLTSExplicit();
  
    IVXBTSExplicit low = factory.createIVXBTSExplicit();
    low.setValue(dateTimeformatter.format((serviceStart != null ? serviceStart: new Date())));
  
    IVXBTSExplicit high = factory.createIVXBTSExplicit();
    high.setValue(dateTimeformatter.format((serviceEnd != null?serviceEnd:new Date())));
  
    effectiveTime.getContent().add(factory.createIVLTSExplicitLow(low));
    effectiveTime.getContent().add(factory.createIVLTSExplicitHigh(high));
  
    serviceEvent.setEffectiveTime(effectiveTime);
    documentationOf.setServiceEvent(serviceEvent);
  
    result.getDocumentationOf().add(documentationOf);
  }

  @Override
  public void buildStructuredBodySection() {
    POCDMT000040Component2 component = new POCDMT000040Component2();
    component.setTypeCode(ActRelationshipHasComponent.COMP);
    component.setContextConductionInd(Boolean.TRUE);
    POCDMT000040StructuredBody structuredBody = new POCDMT000040StructuredBody();
  
    structuredBody.getClassCode().add(ActClassOrganizer.DOCBODY.value());
    structuredBody.getMoodCode().add(XDocumentActMood.EVN.value());
    component.setStructuredBody(structuredBody);
  
    result.setComponent(component);
  }

  private void addTemplateIds(List<II> template, List<String> ids) {
    for (String id : ids) {
      addTemplateIds(template, id);
    }
  }

  /**
   * Appended template id's easily
   * @param template
   * @param id
   */
  private void addTemplateIds(List<II> template, String id) {
    II ii = new II();
    ii.setRoot(id);
    template.add(ii);
  }

  /**
   * Create organizer
   * @return
   */
  private POCDMT000040Organizer createOrganizer() {
    List<String> templateIds = new ArrayList<String>(Arrays.asList("2.16.840.1.113883.10.20.9.4"));
    POCDMT000040Organizer organizer = new POCDMT000040Organizer();
    organizer.setClassCode(XActClassDocumentEntryOrganizer.CLUSTER);
    organizer.getMoodCode().add(XDocumentActMood.EVN.value());
  
    addTemplateIds(organizer.getTemplateId(), templateIds);
  
    CS cs = new CS();
    cs.setCode("completed");
    organizer.setStatusCode(cs);
    return organizer;
  }

  protected POCDMT000040Entry createEntry(POCDMT000040Organizer organizer) {
    POCDMT000040Entry entry = new POCDMT000040Entry();
    entry.setTypeCode(XActRelationshipEntry.COMP);
    entry.setContextConductionInd(Boolean.TRUE);
    entry.setOrganizer(organizer);
    return entry;
  }

  protected POCDMT000040Organizer createOrganizer(POCDMT000040Participant2 participant) {
    POCDMT000040Organizer organizer = createOrganizer();
    organizer.getParticipant().add(participant);
    return organizer;
  }

  protected POCDMT000040Participant2 createParticipant(String instrumentDevice, String instrumentVersion) {
    POCDMT000040Participant2 participant = new POCDMT000040Participant2();
    participant.getTypeCode().add(ParticipationTargetSubject.SBJ.value());
    participant.setContextControlCode("OP");
    addTemplateIds(participant.getTemplateId(), "2.16.840.1.113883.10.20.9.9");
  
  
    POCDMT000040ParticipantRole participantRole = new POCDMT000040ParticipantRole();
    participantRole.getClassCode().add(RoleClassManufacturedProduct.MANU.value());
  
    POCDMT000040Device playingDevice = new POCDMT000040Device();
    playingDevice.setClassCode(EntityClassDevice.DEV);
    playingDevice.setDeterminerCode("INSTANCE");
    SCExplicit value = new SCExplicit();
  
    value.getContent().add(instrumentDevice);
    playingDevice.setManufacturerModelName(value);
  
  
    SCExplicit soft = new SCExplicit();
    soft.getContent().add(instrumentVersion);
    playingDevice.setSoftwareName(soft);
  
    participantRole.setPlayingDevice(playingDevice);
    participant.setParticipantRole(participantRole);
    return participant;
  }

  /**
   * Create PNExplicit structure (name)
   * @param personIdentity The PersonIdentity class to build name structure for
   * @return the PNExplicit element
   */
  protected PNExplicit generateName(PersonIdentity personIdentity) {
      org.hl7.v3.ObjectFactory factory = new org.hl7.v3.ObjectFactory();
      PNExplicit name = (PNExplicit) (factory.createPNExplicit());
  
      if (personIdentity.getFamilyName() != null || personIdentity.getGivenNames() != null) {
  
        if (personIdentity.hasPrefix()) {
          EnExplicitPrefix prefix = new EnExplicitPrefix();
          prefix.setContent(personIdentity.getPrefix());
          name.getContent().add(factory.createPNExplicitPrefix(prefix));
        }
  
  
        for (int i = 0; i < personIdentity.getGivenNames().length; i++) {
          EnExplicitGiven givenName = new EnExplicitGiven();
  //                givenName.setPartType("GIV");
          givenName.setContent(personIdentity.getGivenNames()[i]);
          name.getContent().add(factory.createPNExplicitGiven(givenName));
        }
  
        if (personIdentity.getFamilyName() != null) {
          EnExplicitFamily familyName = new EnExplicitFamily();
  //                familyName.setPartType("FAM");
          familyName.setContent(personIdentity.getFamilyName());
          name.getContent().add(factory.createPNExplicitFamily(familyName));
        }
  
      } else {
        name.getNullFlavor().add("NI");
      }
  
      return name;
    }

  /**
   * Generic method for create telecom
   * @param address
   * @param protocol
   * @param use
   * @return
   */
  private TELExplicit generateTelecom(String address, String protocol, String use) {
    TELExplicit phone = new TELExplicit();
  
    if (address != null) {
      phone.setValue(protocol + ":" + address);
      phone.getUse().add(use);
  
    } else {
      phone.getNullFlavor().add("NI");
    }
    return phone;
  }

  private String translateToHL7String(AddressData.Use use) {
    String useString = "ERROR";
    if (use == AddressData.Use.HomeAddress) {
      useString = "H";
    }
    if (use == AddressData.Use.WorkPlace) {
      useString = "WP";
    }
    return useString;
  }

  /**
   * Utility method to generate Titel element
   * @param titelString
   * @return
   */
  protected STExplicit generateTitle(String titelString) {
    STExplicit title = new STExplicit();
    title.getContent().add(titelString);
    return title;
  }



}