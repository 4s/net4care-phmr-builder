package org.net4care.phmr.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/** Testing various boundary value cases of the
 * data classes.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public class TestUnhappyPaths {

  @Before
  public void setUp() throws Exception {
  }

  @Test
  public void shouldHandlePersonWithOnlyNames() {
    PersonIdentity pi;
    pi = new PersonIdentity.PersonBuilder("Hansen").
        addGivenName("Hans").build();
    String asString = pi.toString();
    assertNotNull(asString);
    assertEquals("PersonIdentity: G: Undifferentiated ID: null (Hansen,[Hans/] Prefix: null Birth: null Adr: (null))", 
        asString);
  }

}
