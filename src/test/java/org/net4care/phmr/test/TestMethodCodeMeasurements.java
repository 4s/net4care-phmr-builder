package org.net4care.phmr.test;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.*;
import org.net4care.core.CoreClinicalDocument;
import org.net4care.phmr.builders.DanishPHMRBuilder;
import org.net4care.phmr.model.*;

import javax.xml.bind.JAXBException;

public final class TestMethodCodeMeasurements {
  private String asString;

  private SimpleClinicalDocument cda;

  @Before
  public void setup() {
    // Step 1. Define the Medcom EX1 CDA
    cda = SetupMedcomExample1.defineAsCDA();
  }

  @Test
  public void shouldValidateMedComCodesForEx1() {
    Context context = new Context(Context.ProvisionMethod.Electronically, Context.PerformerType.Citizen);

      try {
          asString = buildXMLStringForCDAWithContextMeasurement(context);
      } catch (JAXBException e) {
          fail ("Caught exception " + e.getLocalizedMessage());
          e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
      }

    // System.out.println(asString);

    assertTrue("methodCode (Measurer 1) missing or wrong",
        asString.contains("<methodCode code=\"POT\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (Measurer 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Målt af borger\"/>"));

    assertTrue("methodCode (ProvisionMethod 1) missing or wrong",
        asString.contains("<methodCode code=\"AUT\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (ProvisionMethod 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Måling overført automatisk\"/>"));
  }

  @Test
  public void shouldValidateMedComCodesByHealthcareProfessionals() {
    Context context = new Context(Context.ProvisionMethod.TypedByHealthcareProfessional, Context.PerformerType.HealthcareProfessional);

      try {
          asString = buildXMLStringForCDAWithContextMeasurement(context);
      } catch (JAXBException e) {
          fail ("Caught exception " + e.getLocalizedMessage());
          e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
      }

    assertTrue("methodCode (Measurer 1) missing or wrong",
        asString.contains("<methodCode code=\"PNT\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (Measurer 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Målt af aut. sundhedsperson\"/>"));

    assertTrue("methodCode (ProvisionMethod 1) missing or wrong",
        asString.contains("<methodCode code=\"TPH\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (ProvisionMethod 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Indtastet af aut. sundhedsperson\"/>"));
  }

  @Test
  public void shouldValidateMedComCodesForCaregiver() {
    Context context = new Context(Context.ProvisionMethod.TypedByCareGiver, Context.PerformerType.CareGiver);

      try {
          asString = buildXMLStringForCDAWithContextMeasurement(context);
      } catch (JAXBException e) {
          fail ("Caught exception " + e.getLocalizedMessage());
          e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
      }

    assertTrue("methodCode (Measurer 1) missing or wrong",
        asString.contains("<methodCode code=\"PCG\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (Measurer 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Målt af anden omsorgsperson\"/>"));

    assertTrue("methodCode (ProvisionMethod 1) missing or wrong",
        asString.contains("<methodCode code=\"TPC\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (ProvisionMethod 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Indtastet af anden omsorgsperson\"/>"));
  }

  @Test
  public void shouldValidateMedComCodesForRelative() {
    Context context = new Context(Context.ProvisionMethod.TypedByCitizenRelative, Context.PerformerType.Citizen);

      try {
          asString = buildXMLStringForCDAWithContextMeasurement(context);
      } catch (JAXBException e) {
          fail ("Caught exception " + e.getLocalizedMessage());
          e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
      }

    assertTrue("methodCode (Measurer 1) missing or wrong",
        asString.contains("<methodCode code=\"POT\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (Measurer 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Målt af borger\"/>"));

    assertTrue("methodCode (ProvisionMethod 1) missing or wrong",
        asString.contains("<methodCode code=\"TPR\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (ProvisionMethod 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Indtastet af pårørende\"/>"));
  }

  @Test
  public void shouldValidateMedComCodesForCitizen() {
    Context context = new Context(Context.ProvisionMethod.TypedByCitizen, Context.PerformerType.Citizen);

      try {
          asString = buildXMLStringForCDAWithContextMeasurement(context);
      } catch (JAXBException e) {
          fail ("Caught exception " + e.getLocalizedMessage());
          e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
      }

      // System.out.println(asString);

    assertTrue("methodCode (Measurer 1) missing or wrong",
        asString.contains("<methodCode code=\"POT\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (Measurer 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Målt af borger\"/>"));

    assertTrue("methodCode (ProvisionMethod 1) missing or wrong",
        asString.contains("<methodCode code=\"TPD\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (ProvisionMethod 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Indtastet af borger\"/>"));
  }

  private String buildXMLStringForCDAWithContextMeasurement(Context context) throws JAXBException {
    Measurement bloodpressure;
    Date when = HelperMethods.makeDanishDateTime(2014, 0, 14, 9, 45, 00);
    bloodpressure = 
        new Measurement.MeasurementBuilder(when, Measurement.Status.COMPLETED).
        setPhysicalQuantity("138.0", "mm[Hg]", "DNK05472", "Blodtryk systolisk;Arm").
        setContext(context).
        build();

    cda.addResult(bloodpressure);

    asString = buildXMLStringForCDA(cda);
    return asString;
  }

  private String buildXMLStringForCDA(CoreClinicalDocument cda) throws JAXBException {
    // 2. Convert it into a Danish PHMR XML format
    DanishPHMRBuilder builder = new DanishPHMRBuilder(new StubUUIDStrategy("aa2386d0-79ea-11e3-981f-0800200c9a66"));
    cda.construct(builder);
    // 3. Extract the acutal XML DOM and string representation

    // Convert it to string
    asString = builder.getDocumentAsString();

    return asString;
  }
}
