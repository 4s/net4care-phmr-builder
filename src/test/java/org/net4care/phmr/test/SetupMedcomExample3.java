package org.net4care.phmr.test;

import java.util.*;

import org.net4care.core.CoreClinicalDocument;
import org.net4care.phmr.codes.NPU;
import org.net4care.phmr.model.*;
import org.net4care.phmr.model.AddressData.Use;

public class SetupMedcomExample3 {

  /** Define a CDA for the Medcom example 3. */
  public static CoreClinicalDocument defineAsCDA() {

    // Define the 'time'
    Date documentCreationTime = HelperMethods.makeDanishDateTime(2014, 0, 14, 10, 0, 0);

    // 1. Create a PHMR document as a "Green CDA", that is,
    // a data structure containing only the dynamic data of a CDA.
    SimpleClinicalDocument cda = new DanishPHMRModel();

    // 1.1 Populate with time and version info
    cda.setDocumentVersion("2358344", 1);
    cda.setEffectiveTime(documentCreationTime);

    // 1.2 Populate the document with patient information (from base)

    AddressData ellenAddress = new AddressData.AddressBuilder("8850", "Bjerringbro").
        setCountry("Danmark").
        addAddressLine("Martin Bachs Vej 3").
        setUse(AddressData.Use.HomeAddress).
        build();

    PersonIdentity ellen =
        new PersonIdentity.PersonBuilder("Berggren").
            setGender(PersonIdentity.Gender.Female).
            setBirthTime(1966, Calendar.MAY, 21).
            addGivenName("Ellen").
            setPersonID("2105669996").
            setAddress(ellenAddress).
            addTelecom(Use.HomeAddress, "tel","89123456").
            addTelecom(Use.WorkPlace, "mailto","eb@udkantsdanmark.dk").
            build();

    cda.setPatient(ellen);

    // 1.3 Populate with Author, Custodian, and Authenticator
    OrganizationIdentity custodian =
        new OrganizationIdentity.OrganizationBuilder("88878685").
            setName("Århus Universitetshospital").
            setAddress(new AddressData.AddressBuilder("8200", "Århus").
                setCountry("Danmark").
                setUse(AddressData.Use.WorkPlace).
                addAddressLine("Afdelingen for Obstetrik og Gynækologi").
                addAddressLine("Brendstrupgårdsvej 100").
                build()).
            addTelecom(Use.WorkPlace, "tel","78450000").
            build();

    PersonIdentity author =
        new PersonIdentity.PersonBuilder("Andersen").
            addGivenName("Sarah").
            setPrefix("Jordemoder").
            build();

    PersonIdentity authenticator =
        new PersonIdentity.PersonBuilder("Petersen").
            addGivenName("Peter").
            build();

    cda.setAuthor(custodian, author, documentCreationTime);
    cda.setCustodian(custodian);
    cda.setAuthenticator(custodian, authenticator, documentCreationTime);

    // 1.4 Define the service period
    Date fromTime = HelperMethods.makeDanishDateTime(2014, 0, 14, 9, 45, 0);
    Date toTime = HelperMethods.makeDanishDateTime(2014, 0, 14, 10, 0, 0);
    cda.setDocumentationTimeInterval(fromTime, toTime);

    // 1.5 Add measuring equipment
    MedicalEquipment e1, e2, e3, e4;
    e1 = new MedicalEquipment.MedicalEquipmentBuilder()
        .setMedicalDeviceCode("EPQXXXXX").
        setMedicalDeviceDisplayName("Weight")
        .setManufacturerModelName("Manufacturer: AD Company / Model: 6121ABT1")
        .setSoftwareName("SerialNr: 6121ABT1-987 Rev. 3 / SW Rev. 20144711")
        .build();

    e2 = new MedicalEquipment.MedicalEquipmentBuilder()
        .setMedicalDeviceCode("EPQXXXXX")
        .setMedicalDeviceDisplayName("sphygmomanometer")
        .setManufacturerModelName("Manufacturer: AD Company / Model: AU-767PBT-C")
        .setSoftwareName("SerialNr: AU-767PBT-C Rev. 2 / SW Rev. 45144723")
        .build();


    e3 = new MedicalEquipment.MedicalEquipmentBuilder()
        .setMedicalDeviceCode("EPQXXXXX")
        .setMedicalDeviceDisplayName("Urine Analyzer")
        .setManufacturerModelName(
            "Manufacturer: Roche Diagnostics / Model: Urisys 1100")
        .setSoftwareName("SerialNr: GD-226789-F Rev. 4.7 / SW Rev. 23980")
        .build();

    e4 = new MedicalEquipment.MedicalEquipmentBuilder()
        .setMedicalDeviceCode("EPQXXXXX")
        .setMedicalDeviceDisplayName("Fetal Monitor (Monica)")
        .setManufacturerModelName(
            "Manufacturer: GE Healthcare / Model: Corometrics 170")
        .setSoftwareName("SerialNr: GE-114567CRM-C Rev. 4 / SW Rev. 743214")
        .build();
    
    cda.addMedicalEquipment(e1); cda.addMedicalEquipment(e2);
    cda.addMedicalEquipment(e3); cda.addMedicalEquipment(e4);

    // 1.6 Add measurements (observations)

    // Example 2: Use the helper methods to easily create measurements
    // for commonly used telemedical measurements, here examplified
    // by weight. Note - no codes, displaynames, nor UCUM units are
    // given.
    Context context = new Context(Context.ProvisionMethod.Electronically, Context.PerformerType.Citizen, null);

    Measurement systolic = NPU.createBloodPresureSystolic("138", fromTime, context);
    cda.addVitalSign(systolic);

    Measurement diastolic = NPU.createBloodPresureDiastolic("91", fromTime, context);
    cda.addVitalSign(diastolic);

    Measurement weight = NPU.createWeight("75.0", fromTime, context);
    cda.addResult(weight);

    Measurement protein = NPU.createProteinUrine("0", fromTime, context);
    cda.addResult(protein);

    Comment comment = new Comment(author, custodian, toTime, "CTG-målingen ser helt normal ud, intet at bemærke. SA");
    Measurement ctg = NPU.createCTG("484ff720-8f2c-11e3-baa8-0800200c9a66", "ctg_obs/484ff720-8f2c-11e3-baa8-0800200c9a66.png", fromTime, null);
    ctg.setComment(comment);
    cda.addResult(ctg);

    return cda;
  }
}
