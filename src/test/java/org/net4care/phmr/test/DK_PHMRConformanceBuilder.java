package org.net4care.phmr.test;

import java.util.*;

import org.net4care.core.Section;
import org.net4care.phmr.model.*;

/** This is a builder which builds a list of conformance issues
 * for a SimpleClinicalDocument, referring to the Danish standard.
 * 
 * This is certainly work in progress!
 * 
 * Usage:
 * (Pending - see the test cases)
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public final class DK_PHMRConformanceBuilder implements ClinicalDocumentBuilder {
  private List<String> errorList;
  private boolean isValid;

  public DK_PHMRConformanceBuilder() {
    errorList = new ArrayList<String>();
    isValid = true;
  }

  @Override
  public void buildRootNode() {
  }

  @Override
  public void buildHeader(CDAHeaderData data) {
  }

  @Override
  public void buildContext(String title, PersonIdentity patientIdentity, Date effectiveTime,
      String setId, String versionNumber) {
  }

  @Override
  public void buildPatientSection(PersonIdentity patientIdentity) {
    checkIfNameIsWellformed(patientIdentity, "patient");
    checkIfAddressIsWellformed(patientIdentity, "patient");
  }

  @Override
  public void buildStructuredBodySection() {
  }

  @Override
  public void buildAuthorSection(
      OrganizationIdentity authorOrganizationIdentity,
      PersonIdentity authorIdentity, Date authorParticipationStartTime) {
    checkIfNameIsWellformed(authorIdentity, "author");
  }

  @Override
  public void buildCustodianSection(OrganizationIdentity custodianIdentity) {
  }

  @Override
  public void buildLegalAuthenticatorSection(
      OrganizationIdentity authenticatorOrganizationIdentity,
      PersonIdentity authenticatorPersonIdentity, Date timeOfAuthentication) {
  }

  @Override
  public void buildDocumentationOf(Date serviceStart, Date serviceEnd) {
  }

  public boolean isValid() {
    return isValid;
  }

  public List<String> errorList() {
    return errorList;
  }


  public String toString() {
    String result = "DK PHMR version 1.0 conformance check.\n";
    result += "Validity is: " + isValid + "\n";
    for (String line : errorList()) {
      result += " -> " + line + "\n";
    }
    return result;
  }

  private void checkIfNameIsWellformed(PersonIdentity personIdentity, String personRoleName) {
    if (personIdentity == null) {
      errorList.add("The " + personRoleName + " identity is not set (null)");
      isValid = false; return;
    }
    if (personIdentity.getGivenNames() == null
        || personIdentity.getGivenNames().length == 0
        || personIdentity.getGivenNames()[0] == null
        || personIdentity.getGivenNames()[0].equals("")) {
      errorList.add("The " + personRoleName + "'s first name is missing or empty");
      isValid = false; return;
    }
    if (personIdentity.getFamilyName() == null
        || personIdentity.getFamilyName().equals("")) {
      errorList.add("The " + personRoleName + "'s family name is missing or empty");
      isValid = false; return;
    }
  }

  private void checkIfAddressIsWellformed(PersonIdentity patientIdentity,
      String addressRole) {
    if (patientIdentity == null) return;
    AddressData adr = patientIdentity.getAddress(); 
    if(adr == null) {
      errorList.add("No address supplied / Section: "+addressRole); 
      isValid = false;
      return;
    }
    if (adr.getPostalCode() == null ||
        adr.getPostalCode().equals("")) {
      errorList.add("CONF-PHMR-DK- 11: Address SHALL contain exactly one postalcode / Section: "+addressRole);
      isValid = false; 
    } 
  }

  @Override
  public void buildCopyrightSection(String text) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void buildSections(List<Section> sectionList) {
    // TODO Auto-generated method stub
    
  }
}
