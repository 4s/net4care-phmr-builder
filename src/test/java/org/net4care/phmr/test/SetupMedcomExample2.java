package org.net4care.phmr.test;

import java.util.Date;

import org.net4care.core.CoreClinicalDocument;
import org.net4care.phmr.codes.NPU;
import org.net4care.phmr.model.*;
import org.net4care.phmr.model.AddressData.Use;
import org.net4care.phmr.model.Measurement.Status;

public class SetupMedcomExample2 {

  /** Define a CDA for the Medcom example 2. */
  public static CoreClinicalDocument defineAsCDA() {

    // Define the 'time'
    Date documentCreationTime = HelperMethods.makeDanishDateTime(2014, 0, 21, 10, 30, 0);

    // 1. Create a PHMR document as a "Green CDA", that is,
    // a data structure containing only the dynamic data of a CDA.
    SimpleClinicalDocument cda = new DanishPHMRModel();

    // 1.1 Populate with time and version info
    cda.setDocumentVersion("2358344", 1);
    cda.setEffectiveTime(documentCreationTime);

    // 1.2 Populate the document with patient information (from base)
    PersonIdentity nancy = Setup.defineNancyAsFullPersonIdentity();
    cda.setPatient(nancy);

    // 1.3 Populate with Author, Custodian, and Authenticator
    PersonIdentity andersAndersen =
        new PersonIdentity.PersonBuilder("Andersen").
            addGivenName("Anders").
            setPrefix("Hjertelæge").
            build();

    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity svendborgHjerteMedicinskAfdeling  =
        new OrganizationIdentity.OrganizationBuilder("88878685").
            setName("Odense Universitetshospital - Svendborg Sygehus").
            setAddress(Setup.defineHjerteMedicinskAfdAddress()).
            addTelecom(Use.WorkPlace, "tel","65223344").
            build();

    PersonIdentity mathildeChristesen =
        new PersonIdentity.PersonBuilder("Christensen").addGivenName("Mathilde").
            setPrefix("Hjemmesygeplejerske").build();

    OrganizationIdentity hjemmeplejen =
        new OrganizationIdentity.OrganizationBuilder("328151000016009").
            setName("Hjemmesygeplejen, Svendborg kommune").
            setAddress(new AddressData.AddressBuilder("5700", "Svendborg").
                addAddressLine("Svinget 14").
                setCountry("Danmark").
                setUse(AddressData.Use.WorkPlace).
                build()).
            build();

    /*
      new OrganizationIdentity("328151000016009",
        "Hjemmesygeplejen, Svendborg kommune", null,
        new AddressData.AddressBuilder("5700","Svendborg").
        addAddressLine("Svinget 14").
        setCountry("Danmark").
        setUse(AddressData.Use.WorkPlace).
        build());*/
    Date timeOfAuthor = HelperMethods.makeDanishDateTime(2014, 0, 24, 7, 53, 0);
    cda.setAuthor(hjemmeplejen, mathildeChristesen, timeOfAuthor);
    cda.setCustodian(svendborgHjerteMedicinskAfdeling);
    Date timeOfAuthentication = HelperMethods.makeDanishDateTime(2014, 0, 21, 10, 30, 0);
    cda.setAuthenticator(svendborgHjerteMedicinskAfdeling, andersAndersen, timeOfAuthentication);

    // 1.4 Define the service period
    Date fromTime = HelperMethods.makeDanishDateTime(2014, 0, 20, 7, 53, 0);
    Date toTime = HelperMethods.makeDanishDateTime(2014, 0, 20, 14, 25, 0);
    cda.setDocumentationTimeInterval(fromTime, toTime);

    // 1.5 Add measuring equipment
    MedicalEquipment
        e1 =  new MedicalEquipment.MedicalEquipmentBuilder().
        setMedicalDeviceCode("EPQXXXXX").
        setMedicalDeviceDisplayName("Weight").
        setManufacturerModelName("Manufacturer: AD Company / Model: 6121ABT1").
        setSoftwareName("SerialNr: 6121ABT1-987 Rev. 3 / SW Rev. 20144711").
        build();

    MedicalEquipment e2 = new MedicalEquipment.MedicalEquipmentBuilder()
        .setMedicalDeviceCode("EPQXXXXX")
        .setMedicalDeviceDisplayName("Blood Pressure Monitor")
        .setManufacturerModelName("Manufacturer: AD Company / Model: AU-767PBT-C")
        .setSoftwareName("SerialNr: AU-767PBT-C Rev. 2 / SW Rev. 45144723")
        .build();

    cda.addMedicalEquipment(e1);
    cda.addMedicalEquipment(e2);

    // 1.6 Add measurements (observations)

    // Example 2: Use the helper methods to easily create measurements
    // for commonly used telemedical measurements, here examplified
    // by weight. Note - no codes, displaynames, nor UCUM units are
    // given.
    Context context = new Context(Context.ProvisionMethod.TypedByHealthcareProfessional, Context.PerformerType.HealthcareProfessional);
    Comment comment = new Comment(andersAndersen, svendborgHjerteMedicinskAfdeling, HelperMethods.makeDanishDateTime(2014, 0, 21, 10, 40, 0),
        "Jeg kan se at denne måling er tastet ind af Hjemmesygeplejerske Mathilde Christensen. "
            + "Har mistanke om, at den høje værdi skyldes en indtastningsfejl. Målingen kan ikke godkendes. AA");
    Measurement systolic = NPU.createBloodPresureSystolic("253", toTime, context);
    systolic.setStatus(Status.NULLIFIED);
    systolic.setComment(comment);
    cda.addVitalSign(systolic);

    Measurement diastolic = NPU.createBloodPresureDiastolic("86", toTime, context);
    diastolic.setStatus(Status.NULLIFIED);
    cda.addVitalSign(diastolic);

    context = new Context(Context.ProvisionMethod.Electronically, Context.PerformerType.Citizen);
    Measurement weight = NPU.createWeight("77.3", fromTime, context);
    cda.addResult(weight);

    return cda;
  }
}
