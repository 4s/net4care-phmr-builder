package org.net4care.phmr.test;

import java.util.*;

import org.net4care.phmr.codes.*;
import org.net4care.phmr.model.*;
import org.net4care.phmr.model.AddressData.Use;

/** Helper methods to create SimpleClinicalDocuments that
 * match those of MedCom's example 1.
 *
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public class SetupMedcomExample1 {

  /** Define a CDA for the Medcom example 1. */
  public static SimpleClinicalDocument defineAsCDA() {
    SimpleClinicalDocument cda = defineAsCDAWitoutMedicalEquipment();

    // 1.5 Add measuring equipment
    MedicalEquipment equipment = new MedicalEquipment.MedicalEquipmentBuilder().
        setMedicalDeviceCode("EPQ12225").
        setMedicalDeviceDisplayName("Weight").
        setManufacturerModelName("Manufacturer: AD Company / Model: 6121ABT1").
        setSoftwareName("SerialNr: 6121ABT1-987 Rev. 3 / SW Rev. 20144711").
        build();
    cda.addMedicalEquipment(equipment);

    return cda;
  }

  /** Define a CDA for the Medcom example 1 but without medical equipment section. */
  public static SimpleClinicalDocument defineAsCDAWitoutMedicalEquipment() {

    // Define the 'time'
    Date documentCreationTime = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    // Setup Anders Andersen as authenticator
    PersonIdentity andersAndersen =
        new PersonIdentity.PersonBuilder("Andersen").
            addGivenName("Anders").
            build();

    // 1. Create a PHMR document as a "Green CDA", that is,
    // a data structure containing only the dynamic data
    // of a CDA.
    SimpleClinicalDocument cda = new DanishPHMRModel();

    // 1.1 Populate with time and version info
    cda.setDocumentVersion("2358344", 1);
    cda.setEffectiveTime(documentCreationTime);

    // 1.2 Populate the document with patient information  
    PersonIdentity nancy = Setup.defineNancyAsFullPersonIdentity();
    cda.setPatient(nancy);

    // 1.3 Populate with Author, Custodian, and Authenticator
    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity svendborgHjerteMedicinskAfdeling  =
        new OrganizationIdentity.OrganizationBuilder("88878685").
            setName("Odense Universitetshospital - Svendborg Sygehus").
            setAddress(Setup.defineHjerteMedicinskAfdAddress()).
            addTelecom(Use.WorkPlace, "tel","65223344").
            build();
    cda.setAuthor(svendborgHjerteMedicinskAfdeling, andersAndersen, documentCreationTime);
    cda.setCustodian(svendborgHjerteMedicinskAfdeling);
    Date at1000onJan13 = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);
    cda.setAuthenticator(svendborgHjerteMedicinskAfdeling, andersAndersen, at1000onJan13);

    // 1.4 Define the service period
    Date from = HelperMethods.makeDanishDateTime(2014, 0, 6, 8, 2, 0);
    Date to = HelperMethods.makeDanishDateTime(2014, 0, 10, 8, 15, 0);
    cda.setDocumentationTimeInterval(from, to);

    // 1.6 Add measurements (observations)

    // Example 1: Use the helper methods to easily create measurements
    // for commonly used telemedical measurements, here examplified
    // by weight. Note - no codes, displaynames, nor UCUM units are
    // given.
    Date time1 = HelperMethods.makeDanishDateTime(2014, 0, 6, 8, 2, 0);
    Context context = new Context(Context.ProvisionMethod.Electronically, Context.PerformerType.Citizen);
    Measurement weight1 = NPU.createWeight("77.5", time1, context);
    cda.addResult(weight1);

    // Use the basic methods that allow any legal
    // code system to be used but requires all data to be
    // provided
    Date time2 = HelperMethods.makeDanishDateTime(2014, 0, 8, 7, 45, 0);
    Measurement weight2 = new Measurement.MeasurementBuilder(time2, Measurement.Status.COMPLETED).
        setPhysicalQuantity("77.0", UCUM.kg, NPU.DRY_BODY_WEIGHT_CODE, NPU.DRY_BODY_WEIGHT_DISPLAYNAME).
        setContext(context).
        build();
    cda.addResult(weight2);

    Date time3 = HelperMethods.makeDanishDateTime(2014, 0, 10, 8, 15, 0);
    Measurement weight3 = new Measurement.MeasurementBuilder(time3, Measurement.Status.COMPLETED).
        setContext(context).
        setPhysicalQuantity("77.2", UCUM.kg, NPU.DRY_BODY_WEIGHT_CODE, NPU.DRY_BODY_WEIGHT_DISPLAYNAME).
        build();
    cda.addResult(weight3);

    return cda;
  }

}
