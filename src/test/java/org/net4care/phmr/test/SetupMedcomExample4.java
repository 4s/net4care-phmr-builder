package org.net4care.phmr.test;

import java.util.*;

import org.net4care.core.CoreClinicalDocument;
import org.net4care.phmr.codes.DAK;
import org.net4care.phmr.codes.NPU;
import org.net4care.phmr.model.*;

public class SetupMedcomExample4 {

  /** Define a CDA for the Medcom example 4. */
  public static CoreClinicalDocument defineAsCDA() {

    // Define the 'time'
    Date documentCreationTime = HelperMethods.makeDanishDateTime(2013, 01, 17, 9, 15, 0);

    // 1. Create a PHMR document as a "Green CDA", that is,
    // a data structure containing only the dynamic data of a CDA.
    SimpleClinicalDocument cda = new DanishPHMRModel();

    // 1.1 Populate with time and version info
    cda.setDocumentVersion("2358344", 1); // TODO: move to codes
    cda.setEffectiveTime(documentCreationTime);

    // 1.2 Populate the document with patient information (from base)
    PersonIdentity janus =
        new PersonIdentity.PersonBuilder("Berggren").
            setGender(PersonIdentity.Gender.Male).
            setBirthTime(1948, Calendar.JUNE, 26).
            addGivenName("Janus").
            setPersonID("2606481234").
            setAddress(Setup.defineNancyAddress()).
            addTelecom(AddressData.Use.HomeAddress, "tel","65123456").
            addTelecom(AddressData.Use.WorkPlace, "mailto","jab@udkantsdanmark.dk").
            build();

    cda.setPatient(janus);

    // 1.3 Populate with Author, Custodian, and Authenticator
    OrganizationIdentity custodian =
        new OrganizationIdentity.OrganizationBuilder("77668685").
            setName("Odense Universitetshospital").
            setAddress(new AddressData.AddressBuilder("5000", "Odense C").
                setCountry("Danmark").
                setUse(AddressData.Use.WorkPlace).
                addAddressLine("Lungemedicinsk afdeling J").
                addAddressLine("Sdr. Boulevard 29,").
                addAddressLine("Indgang 87-88").
                build()).
            build();

    PersonIdentity author =
        new PersonIdentity.PersonBuilder("Madsen").
            addGivenName("M").
            setPrefix("Læge").
            build();

    PersonIdentity authenticator =
        new PersonIdentity.PersonBuilder("Olsen").
            addGivenName("Lars").
            setPrefix("Læge").
            build();

    cda.setAuthor(custodian, author, documentCreationTime);
    cda.setCustodian(custodian);
    cda.setAuthenticator(custodian, authenticator, documentCreationTime);

    // 1.4 Define the service period
    Date fromTime = HelperMethods.makeDanishDateTime(2013, 1, 11, 9, 11, 0);
    Date toTime = HelperMethods.makeDanishDateTime(2013, 1, 16, 11, 14, 0);
    cda.setDocumentationTimeInterval(fromTime, toTime);

    // 1.5 Add measuring equipment
    MedicalEquipment e1 = new MedicalEquipment.MedicalEquipmentBuilder().
        setMedicalDeviceCode("EPQXXXXX").
        setMedicalDeviceDisplayName("COPD Kit").
        setManufacturerModelName("Manufacturer: AD Company / Model: COHM-0034").
        setSoftwareName("SerialNr: 6788-0034987 / SW Rev. 6755-ABT").
        build();
    
    cda.addMedicalEquipment(e1);

    // 1.6 Add measurements (observations)

    // Example 2: Use the helper methods to easily create measurements
    // for commonly used telemedical measurements, here examplified
    // by weight. Note - no codes, displaynames, nor UCUM units are
    // given.
    Context context = new Context(Context.ProvisionMethod.Electronically, Context.PerformerType.Citizen, null);

    cda.addVitalSign(NPU.createSaturation("92", HelperMethods.makeDanishDateTime(2013, 1, 11, 9, 11, 0), context));
    cda.addVitalSign(NPU.createSaturation("91", HelperMethods.makeDanishDateTime(2013, 1, 12, 8, 45, 0), context));
    cda.addVitalSign(NPU.createSaturation("94", HelperMethods.makeDanishDateTime(2013, 1, 13, 9, 15, 0), context));
    cda.addVitalSign(NPU.createSaturation("95", HelperMethods.makeDanishDateTime(2013, 1, 14, 9, 45, 0), context));
    cda.addVitalSign(NPU.createSaturation("90", HelperMethods.makeDanishDateTime(2013, 1, 15, 7, 1, 0), context));
    cda.addVitalSign(NPU.createSaturation("94", HelperMethods.makeDanishDateTime(2013, 1, 16, 11, 10, 0), context));

    // use DAK-E code system
    cda.addResult(DAK.createFVC("2.5", HelperMethods.makeDanishDateTime(2013, 1, 11, 9, 14, 0), context));
    cda.addResult(DAK.createFVC("2.4", HelperMethods.makeDanishDateTime(2013, 1, 12, 8, 50, 0), context));
    cda.addResult(DAK.createFVC("2.9", HelperMethods.makeDanishDateTime(2013, 1, 13, 9, 21, 0), context));
    cda.addResult(DAK.createFVC("3.1", HelperMethods.makeDanishDateTime(2013, 1, 14, 9, 50, 0), context));
    cda.addResult(DAK.createFVC("2.2", HelperMethods.makeDanishDateTime(2013, 1, 15, 7, 10, 0), context));
    cda.addResult(DAK.createFVC("3.0", HelperMethods.makeDanishDateTime(2013, 1, 16, 11, 14, 0), context));

    return cda;
  }
}
