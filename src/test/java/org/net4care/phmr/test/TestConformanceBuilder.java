package org.net4care.phmr.test;

import static org.junit.Assert.*;

import org.junit.*;
import org.net4care.core.CoreClinicalDocument;
import org.net4care.phmr.builders.*;
import org.net4care.phmr.model.*;

/** Test-driven implementation of the validation logic on
 * a SimpleClinicalDocument instance.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public final class TestConformanceBuilder {

  private CoreClinicalDocument cda;
  private DK_PHMRConformanceBuilder validator;
  private OrganizationIdentity svendborgHjerteMedicinskAfdeling;
  private PersonIdentity wrong;

  @Before
  public void setup() {
    cda = SetupMedcomExample1.defineAsCDA();
    address = Setup.defineNancyAddress();
    validator = new DK_PHMRConformanceBuilder();
  }

  /** Simple 'smoke test' of SimpleCDA's
   * toString method.
   */
  @Ignore // TODO: Bro 2.1 experiment, temporarily disabled...
  @Test
  public void shouldToStringSimpleCDA() {
    PlainStringBuilder builder = new PlainStringBuilder();
    cda.construct(builder);
    String result = builder.getResult(); 
    // System.out.println(result);
    assertTrue(result.contains("SimpleClinicalDocument:"));
    assertTrue(result.contains("setId: 2358344 / version: 1"));
    assertTrue(result.contains("Patient: 2512484916: Nancy Berggren"));
    assertTrue(result.contains("Author:"));
    assertTrue(result.contains("- Anders Andersen"));
    assertTrue(result.contains("Odense Universitetshospital"));
    assertTrue(result.contains("0: Mon Jan 06 08:02:00 CET 2014 :: Legeme masse; Pt 77.5 kg"));
    assertTrue(result.contains("1: Wed Jan 08 07:45:00 CET 2014 :: Legeme masse; Pt 77.0 kg"));
    assertTrue(result.contains("Manufacturer: AD Company / Model: 6121ABT1 SerialNr: 6121ABT1-987 Rev. 3 / SW Rev. 20144711"));
  }

  @Test
  public void shouldValidateCorrectSimpleCDA() {
    cda.construct(validator);
    assertTrue(validator.isValid());
  }

  @Test
  public void shouldDetectNullPatient() {
    cda.setPatient(null);
    cda.construct(validator);

    assertFalse("Missing a null patient check", validator.isValid());
    assertEquals("The patient identity is not set (null)", validator.errorList().get(0));
  }

  @Test
  public void shouldDetectMissingOrEmptyGivenNames1() {

    wrong = new PersonIdentity.PersonBuilder("Hansen").
        addGivenName("").
        setAddress(address).
        build();
    cda.setPatient(wrong);
    cda.construct(validator);

    assertFalse("Missing a first name check", validator.isValid());
    assertEquals("The patient's first name is missing or empty", validator.errorList().get(0));
  }

  @Test
  public void shouldDetectMissingOrEmptyGivenNames2() {
    wrong = new PersonIdentity.PersonBuilder("Hansen").
        setAddress(address).
        build();
    cda.setPatient(wrong);
    cda.construct(validator);

    assertFalse("Missing a first name check", validator.isValid());
    assertEquals("The patient's first name is missing or empty", validator.errorList().get(0));

  }

  @Test
  public void shouldDetectMissingOrEmptyGivenNames3() {
    wrong = new PersonIdentity.PersonBuilder("Hansen").
        addGivenName(null).
        setAddress(address).
        build();
    cda.setPatient(wrong);
    cda.construct(validator);

    assertFalse("Missing a first name check", validator.isValid());
    assertEquals("The patient's first name is missing or empty", validator.errorList().get(0));
  }

  @Test
  public void shouldDetectMissingOrEmptyFamilyName1() {
    wrong = new PersonIdentity.PersonBuilder(null).
        addGivenName("Benjamin").
        setAddress(address).
        build();
    cda.setPatient(wrong);
    cda.construct(validator);

    assertFalse("Missing a last name check", validator.isValid());
    assertEquals("The patient's family name is missing or empty", validator.errorList().get(0));
  }

  @Test
  public void shouldDetectMissingOrEmptyFamilyName2() {
    wrong = new PersonIdentity.PersonBuilder("").
        addGivenName("Benjamin").
        setAddress(address).
        build();
    cda.setPatient(wrong);
    cda.construct(validator);

    assertFalse("Missing a last name check", validator.isValid());
    assertEquals("The patient's family name is missing or empty", validator.errorList().get(0));
  }

  @Test
  public void shouldDetectMissingOrEmptyAuthorName1() {
    // as the validation code is reused, less testing is needed to drive impl
    wrong = new PersonIdentity.PersonBuilder(null).
        addGivenName("Benjamin").
        build();
    cda.setAuthor(svendborgHjerteMedicinskAfdeling, wrong, HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0));
    cda.construct(validator);

    assertFalse("Missing a last name check", validator.isValid());
    assertEquals("The author's family name is missing or empty", validator.errorList().get(0));
  }

  @Test
  public void shouldDetectMissingOrEmptyAuthorName2() {
    wrong = new PersonIdentity.PersonBuilder("Hansen").
        addGivenName("").build();
    cda.setAuthor(svendborgHjerteMedicinskAfdeling, wrong, HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0));
    cda.construct(validator);

    assertFalse("Missing a first name check", validator.isValid());
    assertEquals("The author's first name is missing or empty", validator.errorList().get(0));
  }

  @Test
  public void shouldDetectMultipleProblems() {
    wrong = new PersonIdentity.PersonBuilder("Berggren").
        setAddress(address).
        addGivenName("").build();
    cda.setPatient(wrong);

    wrong = new PersonIdentity.PersonBuilder("").
        addGivenName("Benjamin").
        setAddress(address).
        build();
    cda.setAuthor(svendborgHjerteMedicinskAfdeling, wrong, HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0));
    cda.construct(validator);

    // System.out.println(validator.toString());

    assertEquals(2, validator.errorList().size());
  }
  
  // === Address
  private AddressData address;
  @Test
  public void shouldDetectAddressIssues() {
    address = new AddressData.AddressBuilder("9800", "Hjørring").build();
    
    wrong = new PersonIdentity.PersonBuilder("Christensen").
        addGivenName("Mathilde").
        build();
    cda.setPatient(wrong);
    cda.construct(validator);

    // System.out.println(address);
    // System.out.println(validator.toString());
    
    assertFalse("Missing an address check", validator.isValid());
    assertEquals(1, validator.errorList().size());
    assertEquals("No address supplied / Section: patient", validator.errorList().get(0));
  }
}
