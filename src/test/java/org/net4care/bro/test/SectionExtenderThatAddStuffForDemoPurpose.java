package org.net4care.bro.test;


import org.hl7.v3.*;
import org.net4care.core.*;
import org.w3c.dom.*;

/**  This is a 'demo class' that demonstrates the ability of an instance
 * of SectionExtender to extend a section before it is finally added to
 * the document.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public class SectionExtenderThatAddStuffForDemoPurpose implements SectionExtender {

  @Override
  public void extend(Section section, POCDMT000040Component3 POCDComponent, 
      ObjectFactory factory, Document rootDocument) {
    // System.out.println("---> Processing "+ section);
    
    POCDComponent.getSection().setIdAttr("added-section-id");
    POCDComponent.getSection().getTitle().getContent().add("extended section title");
  }

}
