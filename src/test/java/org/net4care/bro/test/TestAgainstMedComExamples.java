package org.net4care.bro.test;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.xml.parsers.*;

import org.custommonkey.xmlunit.*;
import org.junit.Test;
import org.net4care.bro.builder.DanishQFDBuilder;
import org.net4care.core.CoreClinicalDocument;
import org.net4care.phmr.model.DanishQuestionnaireFormDefinitionDocument;
import org.net4care.phmr.test.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class TestAgainstMedComExamples {
  
  @Test
  public void shouldMatchExpectedValueEx4() throws ParserConfigurationException, SAXException, IOException {
    // Create an PHMR document matching MedCom Example 4
    DanishQuestionnaireFormDefinitionDocument cda = TestBROBuilding.fillInHeaderOfQFD();
    TestBROBuilding.addExample4TextQuestionTo(cda);
    
    // Load the MedCom QFDD example 4, MODIFIED 
    shouldMatchExpectedValue(cda, "dd2386d0-79ea-11e3-981f-0800200c9a66", "QFD_Example_4_Text_Question-MODIFIED.xml");
  }

  // TODO: Code duplication from the PHMR test case should be removed
  private void shouldMatchExpectedValue(CoreClinicalDocument cda, String uuid, String filename) throws ParserConfigurationException, SAXException, IOException {

    DanishQFDBuilder phmrBuilder = new DanishQFDBuilder(new StubUUIDStrategy(uuid));

    // MedCom example references a local style sheet
//    phmrBuilder.getDocument().appendChild(pi);

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    // important! http://www.kdgregory.com/index.php?page=xml.parsing
    // factory.setNamespaceAware(true);
    DocumentBuilder xmlBuilder = factory.newDocumentBuilder();

    // Load the MedCom example
    // this requires the XML files to have an UTF-8 BOM
    Document expected = xmlBuilder.parse("src/test/resources/qfdd/" + filename);

    // Generate the XML
    cda.construct(phmrBuilder);
    // Extract the actual XML DOM and string representation
    Document computed = phmrBuilder.getDocument("../Stylesheet/cda.xsl");

//    computed.insertBefore(clinical,pi);

    assertNotNull(computed);
    // Use XMLDfiff for comparison, ignore differences in whitespace
    XMLUnit.setIgnoreWhitespace(true);
    XMLUnit.setIgnoreComments(true);
    XMLUnit.setIgnoreAttributeOrder(true);
    Diff xmlDiff = new Diff(expected, computed);
    StringBuffer assertMessage = new StringBuffer(), detailedMessage = new StringBuffer();
    xmlDiff.overrideDifferenceListener(new DebugDifferenceListener(assertMessage, detailedMessage));
    boolean similar = xmlDiff.similar();


    // For debugging, you can output all the differences!
    if (!similar) {
      String computedXml = HelperMethods.convertXMLDocumentToString(computed);
      String expectedXml = HelperMethods.convertXMLDocumentToString(expected);

      System.out.println("------------- Detailed Message --------------");
      System.out.print(HelperMethods.indentLines(detailedMessage.toString()));
      System.out.println("------------- Expected XML ------------------");
      System.out.print(HelperMethods.indentLines(expectedXml));
      System.out.println("------------- Computed XML ------------------");
      System.out.print(HelperMethods.indentLines(computedXml));
      System.out.println("------------- End of details ----------------");
    }

    assertTrue(assertMessage.toString(), similar);
  }

  private class DebugDifferenceListener implements DifferenceListener {

    private StringBuffer assertMessage;
    private StringBuffer detailedMessage;

    DebugDifferenceListener(StringBuffer assertMessage, StringBuffer detailedMessage) {
      this.assertMessage = assertMessage;
      this.detailedMessage = detailedMessage;
    }

    @Override
    public int differenceFound(Difference diff) {
      detailedMessage.append(diff.toString()).append("\n");
      assertMessage.append(diff.getDescription()).append("\n");
      return RETURN_ACCEPT_DIFFERENCE;
    }

    @Override
    public void skippedComparison(Node node1, Node node2) {

    }
  }

}
