package org.net4care.bro.test;

import org.hamcrest.CoreMatchers;
import org.junit.*;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.*;

import org.net4care.bro.*;
import org.net4care.bro.builder.DanishQFDBuilder;
import org.net4care.bro.doubles.QFDSpyBuilder;
import org.net4care.phmr.codes.NullFlavor;
import org.net4care.phmr.model.*;
import org.net4care.phmr.model.AddressData.Use;
import org.net4care.phmr.test.*;

public class TestBROBuilding {
  
  private String asString;
  private DanishQuestionnaireFormDefinitionDocument qfd;

  @Before
  public void setup() {
    qfd = fillInHeaderOfQFD();
    addExample4TextQuestionTo(qfd);

    DanishQFDBuilder qfdBuilder = new DanishQFDBuilder(new StubUUIDStrategy("dd2386d0-79ea-11e3-981f-0800200c9a66"));
    qfd.construct(qfdBuilder);

    asString = qfdBuilder.getDocumentAsString();
  }
  

  @Test
  public void shouldHandleNIPerson() {
    PersonIdentity pi = new PersonIdentity.PersonBuilder().NoInformation().build();
    assertNotNull(pi);
    assertThat( pi.getNullFlavor(), is(NullFlavor.NO_INFORMATION));
  }
  
  // === Test-drive the QFD document
  
  @Test
  public void shouldCreateCoreQFDDocument() {
    
    // Retrieve a text version of the contents
    QFDSpyBuilder psb = new QFDSpyBuilder();
    qfd.construct(psb);
    
    asString = psb.getResult();
    
    assertNotNull(asString);

    // System.out.println(asString);
    assertThat( asString, containsString("effective time:"));
  }


  /* Smoke test the header */
  @Test
  public void shouldValidateHeader() {
    // DK-QFDD Section 2.2
    assertThat( asString, containsString("<typeId root=\"2.16.840.1.113883.1.3\" extension=\"POCD_HD000040\"/>"));
    assertThat( asString, containsString("<templateId root=\"1.2.208.184.12.1\"/>"));
    
    assertThat( asString, containsString("<code code=\"74468-0\""));
    assertThat( asString, containsString("displayName=\"Form Definition Document\""));
       
    assertThat( asString, containsString("3 OM DIN EPILEPSI"));
    
    assertThat( asString, not(containsString("legalAuthenticator")));
  }

  // Add a simple question
  @Test
  public void shouldValidateSimpleQuestion() {
    
    // System.out.println(asString);
    assertThat( asString, containsString("<title>Sektion 1</title>"));
    assertThat( asString, containsString("<templateId root=\"2.16.840.1.113883.10.20.32.2.1\"/>"));
    
    assertThat( asString, containsString("<code code=\"74468-0\" codeSystem=\"2.16.840.1.113883.6.1\" codeSystemName=\"LOINC\"/>"));

    assertThat( asString, containsString("<text>OM DETTE SKEMA:</text>"));

    assertThat( asString, containsString("<entry typeCode=\"DRIV\""));

    assertThat( asString, containsString("<organizer classCode=\"BATTERY\" moodCode=\"EVN\">"));

    assertThat( asString, containsString("templateId root=\"2.16.840.1.113883.10.20.32.4.1\""));

    assertThat( asString, containsString("sequenceNumber value=\"1\""));
    assertThat( asString, containsString("<observation classCode=\"OBS\" moodCode=\"DEF\">"));
    assertThat( asString, containsString("templateId root=\"2.16.840.1.113883.10.20.32.4.9\""));  
    
    assertThat( asString, containsString("<id root=\"Some-root-OID\" extension=\"ob1\""));
    assertThat( asString, containsString("assigningAuthorityName=\"Some-Assigning-Authority-Name\"/>"));  
    
    assertThat( asString, containsString("<code code=\"q1\" codeSystem=\"Some-Question-OID\" codeSystemName=\"Some-CodeSystem-Name\" displayName=\"Some-Display-Name\">")); 
  }
  
  @Test
  public void shouldValidateSimpleQuestionText() {
    assertThat( asString, containsString("<originalText>Medfører din epilepsi"));
  }    
  
  // TDD the copyright section
  
  @Test
  public void shouldValidateCopyright() {
    // System.out.println(asString);
    
    // Component level testing
    assertThat( asString, containsString("<templateId root=\"2.16.840.1.113883.10.20.32.2.2\"/>"));
    assertThat( asString, containsString("<title>Copyright section</title>"));
    assertThat( asString, containsString("<text/>"));

    assertThat( asString, containsString("<languageCode code=\"en-US\"/>"));

    // Observation pattern testing
    assertThat( asString, containsString("<templateId root=\"2.16.840.1.113883.10.20.32.4.21\"/>"));
    assertThat( asString, containsString("<code code=\"COPY\" codeSystem=\"2.16.840.1.113883.6.1\""));
  }
  
  @Test
  public void shouldValidateCopyrightText() {
    assertThat( asString, containsString("<originalText>Copyright text must be written here"));
  }
  
  // BRO example 3 - multiple choice
  @Test
  public void shouldValidateExample3Contents() {
    // Create something resembling example 3
    qfd = fillInHeaderOfQFD();
    addExample3MultipleChoiceQuestionsTo(qfd);

    DanishQFDBuilder qfdBuilder = 
        new DanishQFDBuilder(new StubUUIDStrategy("dd2386d0-79ea-11e3-981f-0800200c9a66"),
        new SectionExtenderThatAddStuffForDemoPurpose());
    qfd.construct(qfdBuilder);

    asString = qfdBuilder.getDocumentAsString();
    
    // Assert the question part
    // Disabled - due to section extension: assertThat( asString, containsString("<title>Section 1</title>"));
    assertThat( asString, containsString("<text>1. Heart failure"));

    // The oid for multiple choice questions
    assertThat( asString, containsString("<templateId root=\"2.16.840.1.113883.10.20.32.4.8\"/>"));
    
    // Section 1 question 1
    assertThat( asString, containsString("<code code=\"q2\" codeSystem=\"question-codesystem-oid\" codeSystemName=\"Test System Ex4\" displayName=\"Display-Name-Ex4\">")); 
    assertThat( asString, containsString("<originalText>a. Showering/bathing</originalText>"));

    // Section 1 question 2
    assertThat( asString, containsString("<code code=\"q3\" codeSystem=\"question-codesystem-oid\" codeSystemName=\"Test System Ex4\" displayName=\"Display-Name-Ex4\">")); 
    assertThat( asString, containsString("<originalText>b. Walking 1 block on level ground</originalText>"));
    assertThat( asString, containsString("<sequenceNumber value=\"2\"/>"));
    
    // Assert the possible choices to make for question 1
    assertThat( asString, containsString("<value xsi:type=\"CE\" code=\"A1\" codeSystem=\"Some-ChoiceDomain-OID\" codeSystemName=\"Some-CodeSystem-Name\" displayName=\"Extremely Limited\"") );
    assertThat( asString, containsString("<value xsi:type=\"CE\" code=\"A2\" codeSystem=\"Some-ChoiceDomain-OID\" codeSystemName=\"Some-CodeSystem-Name\" displayName=\"Quite a bit Limited\"") );
    assertThat( asString, containsString("<value xsi:type=\"CE\" code=\"A3\" codeSystem=\"Some-ChoiceDomain-OID\" codeSystemName=\"Some-CodeSystem-Name\" displayName=\"Moderately Limited\"") );
    assertThat( asString, containsString("<value xsi:type=\"CE\" code=\"A4\" codeSystem=\"Some-ChoiceDomain-OID\" codeSystemName=\"Some-CodeSystem-Name\" displayName=\"Slightly Limited\"") );
    assertThat( asString, containsString("<value xsi:type=\"CE\" code=\"A5\" codeSystem=\"Some-ChoiceDomain-OID\" codeSystemName=\"Some-CodeSystem-Name\" displayName=\"Not at all Limited\"") );
    assertThat( asString, containsString("<value xsi:type=\"CE\" code=\"A6\" codeSystem=\"Some-ChoiceDomain-OID\" codeSystemName=\"Some-CodeSystem-Name\" displayName=\"Limited for other reasons or did not do the activity\"") );

    // System.out.println(asString);

    // Assert the EntryRelationship section
    assertThat( asString, containsString("<entryRelationship typeCode=\"SUBJ\">"));
    assertThat( asString, containsString("<observation classCode=\"OBS\" moodCode=\"EVN\">"));
    assertThat( asString, containsString("<templateId root=\"2.16.840.1.113883.10.20.32.4.20\"/>"));
    
    // TODO: Reenable this test
    // assertThat( asString, containsString("<code code=\"74467-2\" codeSystem=\"2.16.840.1.113883.6.1\"/>"));
    
    assertThat( asString, containsString("<value xsi:type=\"IVL_INT\""));
    assertThat( asString, containsString("<low value=\"1\"/>"));
    assertThat( asString, containsString("<high value=\"1\"/>"));
    
    // Assert that the section extender can add and extend data in the QFDD
    assertThat( asString, containsString("<section ID=\"added-section-id\""));
    assertThat( asString, containsString("<title>Section 1 extended section title</title>"));
  }
  

  public static DanishQuestionnaireFormDefinitionDocument fillInHeaderOfQFD() {
    DanishQuestionnaireFormDefinitionDocument qfd;
    qfd = new DanishQuestionnaireFormDefinitionDocument();
    
    // Define the title
    qfd.setTitle("SPØRGESKEMA 3 OM DIN EPILEPSI");
    
    // Define the 'time'
    Date documentCreationTime = HelperMethods.makeDanishDateTime(2015, 0, 12, 10, 0, 0);
    qfd.setEffectiveTime(documentCreationTime);

    // Setup Anders Andersen as authenticator
    PersonIdentity andersAndersen =
        new PersonIdentity.PersonBuilder("Andersen").
            addGivenName("Anders").
            build();
    // 1.3 Populate with Author, Custodian, and Authenticator
    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity svendborgHjerteMedicinskAfdeling  =
        new OrganizationIdentity.OrganizationBuilder("88878685").
            setName("Odense Universitetshospital - Svendborg Sygehus").
            setAddress(Setup.defineNeurologiskAfdAddress()).
            addTelecom(Use.WorkPlace, "tel","65112233").
            build();
    qfd.setAuthor(svendborgHjerteMedicinskAfdeling, andersAndersen, documentCreationTime);
    qfd.setCustodian(svendborgHjerteMedicinskAfdeling);
    
    // QFD has no authenticator! 

    // For a QFD, the patient is No Information
    PersonIdentity noPatient = new PersonIdentity.PersonBuilder().NoInformation().build();
    qfd.setPatient(noPatient);
    
    // Create copyright section
    
    qfd.addCopyright("Copyright text must be written here");

    return qfd;
  }

  public static void addExample4TextQuestionTo(
      DanishQuestionnaireFormDefinitionDocument qfd) {
    // Create question sections
    QFDSection section1;
    section1 = new QFDSection("Sektion 1", "OM DETTE SKEMA:");
    section1.addObservation(
        new QFDObservationText("Medfører din epilepsi (anfald/behandling) alvorlige begrænsninger for dig?",
            "ob1", "Some-root-OID", "Some-Assigning-Authority-Name",
            "q1", "Some-Question-OID", "Some-Display-Name", "Some-CodeSystem-Name"));
    
    qfd.addQFDSection(section1);
  }

  private void addExample3MultipleChoiceQuestionsTo(
      DanishQuestionnaireFormDefinitionDocument qfd) {
    
    QFDSection section1;               
    section1 = new QFDSection("Section 1", "1. Heart failure affects different people...");
    
    QFDObservationMultipleChoice question1;
    question1 = new QFDObservationMultipleChoice("a. Showering/bathing", 
        "q2", "question-codesystem-oid","Display-Name-Ex4", "Test System Ex4");
    question1.addAnswerOption("A1", "Some-ChoiceDomain-OID", "Extremely Limited", "Some-CodeSystem-Name");
    question1.addAnswerOption("A2", "Some-ChoiceDomain-OID", "Quite a bit Limited", "Some-CodeSystem-Name");
    question1.addAnswerOption("A3", "Some-ChoiceDomain-OID", "Moderately Limited", "Some-CodeSystem-Name");
    question1.addAnswerOption("A4", "Some-ChoiceDomain-OID", "Slightly Limited", "Some-CodeSystem-Name");
    question1.addAnswerOption("A5", "Some-ChoiceDomain-OID", "Not at all Limited", "Some-CodeSystem-Name");
    question1.addAnswerOption("A6", "Some-ChoiceDomain-OID", "Limited for other reasons or did not do the activity", "Some-CodeSystem-Name");
    
    section1.addObservation(question1);

    section1.addObservation(new QFDObservationMultipleChoice("b. Walking 1 block on level ground", 
        "q3", "question-codesystem-oid","Display-Name-Ex4", "Test System Ex4"));
    
    qfd.addQFDSection(section1);
    
  }

}
