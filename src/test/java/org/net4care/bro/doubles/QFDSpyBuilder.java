package org.net4care.bro.doubles;

import java.util.*;

import org.net4care.core.Section;
import org.net4care.phmr.model.*;

/** A spy builder class to allow inspecting the QFD document.
 * Primarily used during development of the DanishQuestionnaireFormDefinitionDocument.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public class QFDSpyBuilder implements ClinicalDocumentBuilder {

  private String result;

  /** Get the plain string result.
   *
   * @return The plain string result.
   */
  public String getResult() {
    return result;
  }

  @Override
  public void buildRootNode() {
    result = "Danish QFD:\n";
  }

  @Override
  public void buildHeader(CDAHeaderData data) {
  }

  @Override
  public void buildContext(String title, PersonIdentity patientIdentity, Date effectiveTime,
                           String setId, String versionNumber) {
    result += " setId: " + setId + " / version: " + versionNumber + "\n";
    result += " effective time: "+ effectiveTime+"\n";
    result += " patient id: "+patientIdentity.toString()+"\n";
  }

  @Override
  public void buildPatientSection(PersonIdentity patientIdentity) {
  }

   public void buildGenericMeasurement(Measurement m) {
  }


  @Override
  public void buildStructuredBodySection() {

  }

  @Override
  public void buildAuthorSection(
      OrganizationIdentity authorOrganizationIdentity,
      PersonIdentity authorIdentity, Date authorParticipationStartTime) {

    result += " Author: \n";
    result += "  Person: "+authorIdentity.toString()+"\n";
    result += "  Org   : "+authorOrganizationIdentity.toString()+"\n";
  }

  @Override
  public void buildCustodianSection(OrganizationIdentity custodianIdentity) {
    
    result += " Custodian: \n";
    result += custodianIdentity.toString();
  }

  @Override
  public void buildLegalAuthenticatorSection(
      OrganizationIdentity authenticatorOrganizationIdentity,
      PersonIdentity authenticatorPersonIdentity, Date timeOfAuthentication) {
    /*
    result += " Authenticator: \n";
    result += "   " + authenticatorOrganizationIdentity.getOrgName() + "\n";
    result += "    - " + authenticatorPersonIdentity.getGivenNames()[0] + " " + authenticatorPersonIdentity.getFamilyName() + "\n";*/
  }

  @Override
  public void buildDocumentationOf(Date serviceStart, Date serviceEnd) {
  }

  @Override
  public void buildCopyrightSection(String text) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void buildSections(List<Section> sectionList) {
    // TODO Auto-generated method stub
    
  }

}
