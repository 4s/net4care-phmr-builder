Net4Care PHMR Builder Releases
------------------------------

 0.0.7:
  * Enabled deployment on 4S artifactory.

 0.0.6:
  * Added (lots) to support simple QFDD building. This entailed
    quite a lot of abstraction in the PHMR builder, as well
    as adding several new modules and classes.
  * Updated PHMR examples to have the new MedCom OIDs.
  * NOTE: The QFDD model and building is NOT COMPLETE. It is provided
     as a starting point for further work. Refactoring in the
     module structure also pending.

 0.0.5: 
  * Added toString to several SimpleClinicalDocument part objects.
 
 0.0.4: 
  * Clean up of builder interface.

 0.0.3: 
  * Changed MedicalEquipment to use Bloch builder pattern.
  
 0.0.2: 
  * Initial release (September 2014).
 
